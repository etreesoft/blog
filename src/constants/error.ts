export const errorMessage = {
  default: '일시적인 오류입니다. 잠시 후 다시 시도해주세요.',
} as const

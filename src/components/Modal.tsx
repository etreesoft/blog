import { css } from '@emotion/react'

import {
  PropsWithChildren,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react'
import ReactModal from 'react-modal'

import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
import { disableBodyScroll, enableBodyScroll } from '@/utils/bodyScrollLock'
import { injectProps, makeCompoundComponent } from '@/utils/common'

import Icon from './Icon'

type CloseEvent =
  | React.MouseEvent<Element, MouseEvent>
  | React.KeyboardEvent<Element>

export interface ModalProps {
  width?: string | number
  height?: string | number
  minHeight?: string | number
  title: string
  titleExtra?: React.ReactNode
  blindTitle?: boolean
  catchOnCancel?: boolean
  shouldCloseOnOverlayClick?: boolean
  isOpen: boolean
  onClose?: (event: CloseEvent) => void
  sendToModal?: never
  onReceiveFromModal?: (props: unknown) => void
}

function Modal({
  width = 'auto',
  height = 'auto',
  minHeight = 'auto',
  isOpen = false,
  title = '',
  blindTitle = false,
  titleExtra,
  onClose,
  shouldCloseOnOverlayClick = true,
  children,
}: PropsWithChildren<ModalProps>) {
  const modalRef = useRef<ReactModal>(null)

  const [_isOpen, _setIsOpen] = useState(false)

  const handleClose = useCallback(
    (event: CloseEvent) => {
      onClose?.(event)
    },
    [onClose]
  )

  useEffect(() => {
    const modal = modalRef.current
    _setIsOpen(isOpen)

    return () => {
      _setIsOpen(false)
      enableBodyScroll(modal as unknown as Element)
    }
  }, [isOpen, _setIsOpen])

  return (
    <ReactModal
      ref={modalRef}
      isOpen={_isOpen}
      contentLabel={title}
      shouldCloseOnOverlayClick={shouldCloseOnOverlayClick}
      onRequestClose={handleClose}
      onAfterOpen={() =>
        disableBodyScroll(modalRef.current as unknown as Element)
      }
      onAfterClose={() =>
        enableBodyScroll(modalRef.current as unknown as Element)
      }
      overlayElement={({ style, ...props }, contentElement) => (
        <div {...props} css={styles.container({ isOpen })}>
          {contentElement}
        </div>
      )}
      contentElement={({ style, ...props }, children) => (
        <div {...props} css={styles.contents({ width, height, minHeight })}>
          {children}
        </div>
      )}
    >
      <div css={styles.header}>
        {/* TODO: blind 클래스 추가 전 선작업 해놨으므로 실제 공통 blind 클래스 추가되면 확인 필요 */}
        <h1 css={styles.title} className={blindTitle ? 'blind' : undefined}>
          {title}
        </h1>
        {titleExtra}

        <button
          type='button'
          onClick={handleClose}
          css={styles.btnClose}
          children={<Icon icon='close' />}
        />
      </div>

      <div css={styles.content} className='ReactModal__Content'>
        {children}
      </div>
    </ReactModal>
  )
}

const InjectPropsChildren = (children: React.ReactNode) => {
  return injectProps(children, ['Button'], { size: 44 }, 'child')
}

interface ActionProps {
  sticky?: boolean
}

function Action({ children, sticky }: PropsWithChildren<ActionProps>) {
  return (
    <div css={styles.action({ sticky })} className='ReactModal__Action'>
      {InjectPropsChildren(children)}
    </div>
  )
}

function Left({ children }: PropsWithChildren<ActionProps>) {
  return <div css={styles.col}>{InjectPropsChildren(children)}</div>
}

function Right({ children }: PropsWithChildren<ActionProps>) {
  return <div css={styles.col}>{InjectPropsChildren(children)}</div>
}

export default makeCompoundComponent(Modal, {
  Action: makeCompoundComponent(Action, { Left, Right }),
})

const styles = {
  container: ({ isOpen }: { isOpen: boolean }) => css`
    // 아웃라인 숨김
    &,
    & * {
      outline: none;
    }
    &.ReactModal__Overlay {
      box-sizing: border-box;
      display: flex;
      justify-content: center;
      align-items: center;
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 30;
    }

    &.ReactModal__Overlay--after-open {
      ${isOpen &&
      css`
        background: ${Colors.DIMMED};
      `}
    }
  `,
  contents: ({
    width,
    height,
    minHeight,
  }: {
    width: string | number
    height: string | number
    minHeight: string | number
  }) => css`
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 24px 0;
    overflow: auto;
    box-sizing: border-box;
    min-width: 400px;
    max-width: 90vw;
    max-height: 90vh;
    -webkit-overflow-scrolling: auto;
    background-color: #fff;
    border-radius: 8px;
    outline: none;

    width: ${typeof width === 'number' ? `${width}px` : width};
    height: ${typeof height === 'number' ? `${height}px` : height};
    min-height: ${typeof minHeight === 'number' ? `${minHeight}px` : minHeight};

    .ReactModal__Action {
      display: flex;
      justify-content: center;
      gap: 10px;
    }
  `,
  header: css`
    position: relative;
    padding: 32px;
    padding-bottom: 0;
  `,
  title: css`
    margin: 0 10px 0 0;
    ${Fonts.BOLD_20}
  `,
  btnClose: css`
    position: absolute;
    top: 0;
    right: 0;
    padding: 24px 24px 0 10px;
    cursor: pointer;
  `,
  content: css`
    overflow: auto;
    flex-grow: 1;
    flex-shrink: 1;
    padding: 0 32px 16px;
  `,
  action: ({ sticky }: { sticky?: boolean }) => css`
    box-sizing: border-box;
    flex-shrink: 0;
    display: flex;
    justify-content: center;
    padding: 30px;

    /* action 너비가 container 너비와 동일하게 표현되게 처리  */
    margin: 0 -32px -16px;

    ${sticky &&
    css`
      position: sticky;
      /* Action 아래 스크롤 중인 콘텐츠 보이지 않도록 처리 */
      bottom: 0;
      transform: translateY(16px);
      margin-top: -6px;
      background-color: #fff;
      z-index: 10;
    `}
  `,
  col: css`
    > button ~ button {
      margin-left: 8px;
    }

    &[class*='Left'] {
      margin-right: auto;
    }

    &:first-of-type:last-of-type {
      &[class*='Right'] {
        margin-left: auto;
      }
    }
  `,
}

import { css } from '@emotion/react'
import styled from '@emotion/styled/macro'
import _ from 'lodash'

import { useMemo } from 'react'

import { Colors } from '@/styles/colors'
import { Fonts, StyleFontWeight } from '@/styles/fonts'
import { HTMLStyleProps } from '@/types'

import Icon from './Icon'

/**
 * @field page: 현재 페이지
 * @field totalPages: 총 페이지 수
 * @field totalPages > totalCount : 총 아이템 갯수
 * @field totalPages > perPage : 한 페이지 당 아이템 (rows) 갯수
 * @field range: 페이지 범위
 * @field onChange: 페이지 변경 핸들러
 */

type Align = 'left' | 'center' | 'right'

export interface PaginationProps extends HTMLStyleProps {
  page?: number
  totalPages?:
    | {
        totalCount: number
        perPage: 10 | 20 | 30 | 50 | 100
      }
    | number
  range?: number
  onChange?: (page: number) => void
  align?: Align
}

const Pagination = ({
  page = 1,
  totalPages = {
    totalCount: 0,
    perPage: 10,
  },
  range = 10,
  onChange,
  align = 'right',
  ...rest
}: PaginationProps) => {
  // 총 페이지 수 (totalPages 가 있으면, totalItemsCount 와 itemsCountPerPage 로 totalPages 를 계산하지 않고 바로 사용합니다)
  const total = useMemo(() => {
    if (_.isNumber(totalPages)) {
      return totalPages
    }

    const { totalCount, perPage } = totalPages
    return Math.ceil(totalCount / perPage)
  }, [totalPages])

  // 페이지 범위 (range) 단위로 쪼갠 2차원 배열
  const pageChunk = useMemo(
    () => _.chunk(_.range(total), range),
    [total, range]
  )

  // pageChunk 에서 현재 페이지가 속한 범위(배열). (ex. pageChunk[0])
  const pages = pageChunk[~~(page / range) - (page % range === 0 ? 1 : 0)] ?? [
    0,
  ]

  const handleChangePage = (
    e: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
    page: number,
    disabled: boolean
  ) => {
    e.preventDefault()
    if (disabled) {
      return
    }
    onChange?.(page)
  }

  return (
    <PaginationContainer align={align} {...rest}>
      {['first', 'prev', ...pages, 'next', 'last'].map((item, i, list) => {
        const isNumber = _.isNumber(item)
        const isSinglePage = list.filter(_.isNumber).length <= 1

        const buttonPages: { [key: string]: number } = {
          first: 1,
          prev: Math.max(1, page - range),
          next: Math.min(total, page + range),
          last: total,
        }

        const current = isNumber ? item + 1 : buttonPages[item]
        const isCurrent = current === page

        const isDisabled = (() => {
          if (isSinglePage) {
            return true
          }

          if (!isNumber) {
            return item === 'prev' || item === 'first'
              ? page === 1
              : page === total
          }

          return isCurrent
        })()

        return (
          <a
            key={item}
            css={
              isNumber
                ? styles.page({ isCurrent, isDisabled })
                : styles.button({ item, isDisabled })
            }
            onClick={(e) => handleChangePage(e, current, isDisabled)}
            href='#'
          >
            {isNumber && current}
            {!isNumber && (
              <Icon
                icon={
                  item === 'first' || item === 'last'
                    ? 'paginationLast'
                    : 'arrowRight'
                }
              />
            )}
          </a>
        )
      })}
    </PaginationContainer>
  )
}

type StyleProps = {
  align: Align
}

export const PaginationContainer = styled.div<StyleProps>`
  display: flex;
  gap: 4px;
  height: 24px;
  align-items: center;
  justify-content: ${({ align }) => align};
  margin: 20px 0;
`

export const styles = {
  button: ({ item, isDisabled }: { item: string; isDisabled: boolean }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 12px;
    height: 100%;
    cursor: pointer;

    ${isDisabled &&
    css`
      opacity: 0.2;
      cursor: default;
    `}

    ${(item === 'first' || item === 'prev') &&
    css`
      transform: scaleX(-1);
    `}

    ${item === 'next' &&
    css`
      margin-left: 16px;
    `}

    ${item === 'prev' &&
    css`
      margin-right: 16px;
    `}
  `,
  page: ({
    isCurrent,
    isDisabled,
  }: {
    isCurrent: boolean
    isDisabled: boolean
  }) => css`
    box-sizing: border-box;
    min-width: 24px;
    padding: 3px 7px 0;
    text-decoration: none;
    border-radius: 4px;
    text-align: center;
    ${Fonts.REGULAR_14};

    ${isDisabled &&
    css`
      cursor: default;
    `}

    ${isCurrent &&
    css`
      background-color: ${Colors.COOL_GRAY_10};
      font-weight: ${StyleFontWeight.BOLD};
    `}
  `,
}

export default Pagination

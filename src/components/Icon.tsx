import { css } from '@emotion/react'

import { HTMLAttributes } from 'react'

import { Icons } from '@/styles/icons'
import { HTMLStyleProps } from '@/types'

interface IconProps extends HTMLStyleProps, HTMLAttributes<HTMLElement> {
  icon: keyof typeof Icons
  width?: number
  height?: number
}

export default function Icon({ icon, width, height, ...rest }: IconProps) {
  const { defaultSize, source } = Icons[icon] ?? {}

  return (
    <i
      css={styles.icon({
        icon,
        width: width ?? defaultSize.width,
        height: height ?? defaultSize.height,
      })}
      dangerouslySetInnerHTML={{ __html: source }}
      {...rest}
    />
  )
}

const styles = {
  icon: ({ width, height }: IconProps) => css`
    display: inline-flex;
    width: ${width}px;
    height: ${height}px;
    justify-content: center;
    align-items: center;
  `,
}

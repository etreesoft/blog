import { css } from '@emotion/react'

import { ButtonHTMLAttributes, PropsWithChildren } from 'react'

import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
import { Icons, makeIconsBackground } from '@/styles/icons'
import { HTMLStyleProps } from '@/types'

import Icon from '../Icon'

type ButtonSize = 46 | 44 | 40 | 38 | 32 | 26

type IconKey = keyof typeof Icons

type StyleProps = {
  primary?: boolean
  size?: ButtonSize
  icon?: IconKey
  iconPlacement?: 'left' | 'right'
  iconColor?: string
}

export interface ButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLStyleProps,
    StyleProps {
  type?: 'button' | 'submit' | 'reset'
  loading?: boolean
}

const IconMargin: { [key: string]: number } = {
  excel: 6,
  reset: 4,
  arrowPrev: 4,
  arrowNext: 4,
}

export default function Button({
  type = 'button',
  size = 32,
  primary = false,
  icon,
  iconPlacement = 'left',
  iconColor,
  children,
  disabled,
  loading = false,
  ...rest
}: PropsWithChildren<ButtonProps>) {
  const stylesProps = { primary, size, disabled, icon, iconPlacement }

  return (
    <button
      css={styles.button(stylesProps)}
      type={type}
      disabled={loading || disabled}
      {...rest}
    >
      <i css={styles.loading(size, primary, loading)} />
      {children}
      {icon && (
        <Icon
          icon={icon}
          css={styles.icon({
            iconPlacement,
            iconColor,
            margin: IconMargin[icon],
          })}
          style={{ marginTop: '-1px' }}
        />
      )}
    </button>
  )
}

Button.displayName = 'Button'

export const styles = {
  button: ({ primary, size, icon, iconPlacement }: StyleProps) => css`
    display: inline-flex;
    align-items: center;
    justify-content: center;
    vertical-align: middle;
    box-sizing: border-box;
    border-radius: 4px;
    min-width: 108px;
    min-height: ${size}px;
    transition: all 0.3s ease, box-shadow 0.4s ease;
    box-shadow: inset 0 0 0 1px ${Colors.BLACK};
    background-color: #fff;

    /* 폰트 어센더/디센더 대응 */
    padding: 6px 20px 5px;

    /* (GPU 컴포지팅) box-shadow inset transition 잔상 현상 방지 */
    transform: translateZ(0);

    &:not(:disabled):hover {
      box-shadow: inset 0 0 0 2px ${Colors.BLACK};
    }

    & + button {
      margin-left: 10px;
    }

    ${size === 40 &&
    css`
      min-width: 0;
      padding-left: 32px;
      padding-right: 32px;
    `}

    ${(size === 38 || size === 32 || size === 26) &&
    css`
      min-width: 0;
      ${Fonts.REGULAR_14};
    `}

    ${size === 26 &&
    css`
      padding: 3px 12px 2px;
    `}

    ${icon &&
    css`
      padding-left: 9px;
      padding-right: 9px;
    `}
    ${iconPlacement === 'left' &&
    css`
      flex-direction: row-reverse;
    `}

    ${primary &&
    css`
      background-color: ${Colors.BLACK};
      box-shadow: none;
      color: #fff;

      &:not(:disabled):hover {
        box-shadow: none;
        background-color: ${Colors.BLACK_70};
      }
    `}
    
    /* Fonts 기본 line-height override */
    line-height: normal;

    :disabled {
      opacity: 0.2;
      cursor: default;
    }
  `,
  icon: ({
    iconPlacement,
    iconColor,
    margin,
  }: StyleProps & { margin: number }) => css`
    flex-shrink: 0;
    margin-right: ${margin}px;

    ${iconPlacement === 'right' &&
    css`
      margin-left: ${margin}px;
      margin-right: 0;
    `}

    ${iconColor &&
    css`
      color: ${iconColor};
    `}
  `,
  loading: (size: ButtonSize, primary: boolean, loading: boolean) => css`
    position: relative;
    right: 0;
    width: 0;
    height: 0;
    margin-left: 0;
    transition: all 0.3s;
    opacity: 0;

    ${loading &&
    css`
      width: 20px;
      height: 20px;
      margin-left: 8px;
      opacity: 1;
    `}

    ${makeIconsBackground(
      'loadingCircle',
      primary ? '#fff' : Colors.BLACK
    )};

    @-webkit-keyframes loadingCircle {
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes loadingCircle {
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    animation: loadingCircle 1s infinite linear;
  `,
}

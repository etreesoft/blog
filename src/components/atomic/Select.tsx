import { css } from '@emotion/react'
import Downshift from 'downshift'

import {
  Dispatch,
  SetStateAction,
  forwardRef,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'

import { Colors } from '@/styles/colors'
import { Fonts, StyleFontWeight } from '@/styles/fonts'
import { FORM_CONTROL, FORM_CONTROL_STYLE } from '@/styles/form'

import Icon from '../Icon'
import ErrorMessage from './ErrorMessage'

export type Option = {
  value: string | number
  label: string | number
}

type SizeType = 'small' | 'medium' | 'large'

export interface SelectProps {
  placeholder?: string
  value?: string | number
  defaultValue?: string | number
  setValue?: Dispatch<SetStateAction<string | number>> | (() => void)
  isValid?: boolean
  errorMessage?: string
  onChange?: (selection: Option) => void
  autoFocus?: boolean
  size?: SizeType
  disabled?: boolean
  options?: Option[]
  width?: number | string
}

export default forwardRef<HTMLInputElement, SelectProps>(function Select(
  {
    placeholder = '선택해 주세요.',
    value,
    defaultValue,
    options = [],
    setValue = () => undefined,
    onChange = () => undefined,
    isValid,
    errorMessage = '',
    autoFocus = false,
    size = 'medium',
    disabled = false,
    width,
  },
  ref
) {
  const inputEl = useRef<HTMLInputElement>(null)

  useEffect(() => {
    if (autoFocus && inputEl.current) {
      inputEl.current.focus()
    }
  }, [autoFocus])

  const [isOpen, setIsOpen] = useState(false)
  const [label, setLabel] = useState<string | number>('')

  const showError = useMemo(() => !isValid && isValid !== undefined, [isValid])
  const isEmpty = options.length === 0

  useEffect(() => {
    if (!value && !defaultValue) {
      return
    }

    const label = options.find(
      (option) => option.value === value || defaultValue
    )?.label

    setLabel(label || '')
  }, [defaultValue, options, value])

  const handleChange = useCallback(
    (select: Option): void => {
      if (!select?.value) {
        return
      }

      setValue(select.value)
      setLabel(select.label)
      onChange(select)
    },
    [onChange, setValue]
  )

  const handleOpen = useCallback(() => {
    if (disabled) {
      return
    }

    setIsOpen(!isOpen)
  }, [disabled, isOpen])

  return (
    <>
      <Downshift
        onChange={(selection) => handleChange(selection as Option)}
        itemToString={(item) => (item ? item.value : '')}
        isOpen={isOpen}
        onOuterClick={() => setIsOpen(false)}
      >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          selectedItem,
          getRootProps,
          isOpen,
        }) => (
          <div css={styles.container} onClick={handleOpen} data-form>
            <div
              css={styles.selectedContainer({
                width,
                size,
                isValid,
                disabled,
                isOpen,
              })}
              {...getRootProps({}, { suppressRefError: true })}
            >
              <input
                {...getInputProps()}
                ref={ref ?? inputEl}
                type='text'
                readOnly
                placeholder={placeholder}
                value={label}
                css={styles.input(size)}
              />
              <Icon icon='dropdownArrow' aria-hidden />
            </div>

            {!isEmpty && (
              <ul {...getMenuProps()} css={styles.optionList({ isOpen, size })}>
                {options.map((item, index) => (
                  <li
                    {...getItemProps({ key: item.value, index, item })}
                    css={styles.option({
                      size,
                      selected: selectedItem === item,
                    })}
                  >
                    {item.label}
                  </li>
                ))}
              </ul>
            )}
          </div>
        )}
      </Downshift>

      {showError && (
        <ErrorMessage message={errorMessage} css={styles.errorMessage} />
      )}
    </>
  )
})

const { borderColor, bgColor, color } = FORM_CONTROL_STYLE

const styles = {
  container: css`
    display: inline-block;
    position: relative;
    margin-right: 10px;
    vertical-align: middle;
    background-color: #fff;
  `,
  selectedContainer: ({
    isValid,
    disabled,
    width,
    size,
    isOpen,
  }: {
    disabled?: boolean
    isValid?: boolean
    width?: number | string
    size: SizeType
    isOpen?: boolean
  }) => css`
    box-sizing: border-box;
    position: relative;
    width: 138px;
    border-radius: 4px;
    padding-right: 40px;
    transition: all 0.3s ease, box-shadow 0.4s ease;
    box-shadow: inset 0 0 0 1px ${borderColor.primary};
    background-color: #fff;

    /* (GPU 컴포지팅) box-shadow inset transition 잔상 현상 방지 */
    transform: translateZ(0);

    &:hover {
      border-width: 2px;
    }

    /* Icon(dropdown indicator) */
    i[aria-hidden] {
      position: absolute;
      top: 50%;
      right: 14px;
      transform: translateY(-50%);
    }

    ${width &&
    css`
      width: ${width}${typeof width === 'string' ? '' : 'px'};
    `}

    ${isValid === false &&
    css`
      box-shadow: inset 0 0 0 1px ${borderColor.error};
    `}

    ${disabled &&
    css`
      box-shadow: inset 0 0 0 1px ${borderColor.disabled};
      background-color: ${bgColor.disabled};
      color: ${color.disabled};
      input:read-only {
        color: inherit;
      }
    `}
    
    ${!disabled &&
    css`
      &:hover {
        box-shadow: inset 0 0 0 2px ${borderColor.primary};
      }
    `}

    ${size === 'small' &&
    css`
      padding-right: 26px;
      i[aria-hidden] {
        right: 8px;
      }
    `}

    ${isOpen &&
    css`
      i[aria-hidden] {
        transform: translateY(-50%) rotate(180deg);
      }
    `}
  `,
  input: (size: SizeType) => css`
    ${FORM_CONTROL};
    min-height: 38px;
    overflow: hidden;
    width: 100%;
    padding-right: 0;
    text-overflow: ellipsis;
    border: 0;
    cursor: default;
    user-select: none;

    && {
      background-color: transparent;
    }

    &::selection {
      background-color: transparent;
    }

    &:read-only {
      color: ${color.primary};
    }

    ${size === 'small' &&
    css`
      min-height: 32px;
      padding: 4px 0 3px 8px;
    `}

    ${size === 'large' &&
    css`
      min-height: 44px;
    `}
  `,
  optionList: ({ isOpen, size }: { isOpen: boolean; size: SizeType }) => css`
    display: none;
    overflow: hidden;
    position: absolute;
    left: 0;
    right: 0;
    list-style: none;
    margin: 4px 0 0;
    padding: 7px;
    border-radius: 4px;
    border: 1px solid ${Colors.BLACK_20};
    background-color: #fff;
    z-index: 10;

    ${isOpen &&
    css`
      display: block;
    `}

    /* TODO: size , isOpen 옵션 너무 파편화되어있음. emotion styled로 분리 */
    ${size === 'small' &&
    css`
      padding: 5px;
    `}

    ${size === 'large' &&
    css`
      padding: 9px;
    `}
  `,
  option: ({ selected, size }: { size: SizeType; selected: boolean }) => css`
    ${Fonts.REGULAR_14};
    padding: 6px 8px 4px;
    margin-bottom: 10px;
    border-radius: 4px;
    cursor: default;

    &:hover {
      background-color: ${Colors.COOL_GRAY_10};
    }

    &:last-of-type {
      margin-bottom: 0;
    }

    ${size === 'small' &&
    css`
      padding: 5px 6px 3px;
      margin-bottom: 4px;
    `}

    ${size === 'large' &&
    css`
      padding: 9px 12px 7px;
      margin-bottom: 8px;
    `}

    ${selected &&
    css`
      font-weight: ${StyleFontWeight.BOLD};
    `}
  `,
  errorMessage: css`
    margin-top: 4px;
  `,
}

import { css } from '@emotion/react'

import React, {
  Dispatch,
  SetStateAction,
  TextareaHTMLAttributes,
  forwardRef,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react'

import { Fonts } from '@/styles/fonts'
import { FORM_CONTROL, FORM_CONTROL_STYLE } from '@/styles/form'
import { HTMLStyleProps } from '@/types'

import ErrorMessage from './ErrorMessage'

export interface TextareaProps
  extends TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLStyleProps {
  setValue?: Dispatch<SetStateAction<string>> | (() => void)
  isValid?: boolean
  errorMessage?: string
  autoFocus?: boolean
}

export default forwardRef<HTMLTextAreaElement, TextareaProps>(function Textarea(
  {
    value,
    setValue = () => undefined,
    onChange = () => undefined,
    isValid,
    errorMessage = '',
    autoFocus = false,
    style,
    className,
    rows,
    ...rest
  },
  ref
) {
  const inputEl = useRef<HTMLTextAreaElement>(null)

  useEffect(() => {
    if (autoFocus && inputEl.current) {
      inputEl.current.focus()
    }
  }, [autoFocus])

  const showError = useMemo(() => !isValid && isValid !== undefined, [isValid])

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
      const value = e?.target?.value
      setValue(value)
      onChange(e)
    },
    [onChange, setValue]
  )

  const styleProps = { className, style }

  return (
    <>
      <div css={styles.container} {...styleProps} data-form>
        <textarea
          ref={ref ?? inputEl}
          value={value}
          onChange={handleChange}
          css={styles.input({ showError })}
          rows={rows ?? 3}
          {...rest}
        />
      </div>

      {showError && (
        <ErrorMessage message={errorMessage} css={styles.errorMessage} />
      )}
    </>
  )
})

const { borderColor, color } = FORM_CONTROL_STYLE

export const styles = {
  container: css`
    position: relative;
    display: inline-block;
    width: 100%;
    vertical-align: middle;
    line-height: 0;
  `,
  input: ({
    showExtra,
    showError,
  }: {
    showExtra?: boolean
    showError?: boolean
  }) =>
    css`
      ${FORM_CONTROL};
      ${Fonts.REGULAR_14};
      padding: 10px 14px;
      border-color: ${borderColor.primary};
      color: ${color.primary};

      ${showExtra &&
      css`
        padding-right: 30px;
      `}

      ${showError &&
      css`
        border-color: ${borderColor.error};
        color: ${color.error};
      `}
    `,
  errorMessage: css`
    margin-top: -6px;
  `,
}

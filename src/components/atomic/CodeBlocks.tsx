import React, {PropsWithChildren, useState} from 'react'
import { Code,  CodeBlock, CopyBlock } from 'react-code-blocks'

import { Languages, LanguageType, ThemeType, themeComponent } from '@/styles/language'

type StyleProps = {
  codeStyle?: 'block' | 'copy' | 'code'
  language?: LanguageType
  text: string
  showLineNumbers?: boolean
  theme?: ThemeType
  wrapLines?: boolean
  highlight?: string
}

export default function CodeBlocks({
    codeStyle = 'block',
    language = 'bash',
    text,
    showLineNumbers = false,
    theme = 'dracula',
    wrapLines = false,
    highlight,
}: PropsWithChildren<StyleProps>) {
  console.log('')
  return codeStyle === 'block' ?
      <CodeBlock
          language={Languages[language]}
          text={text}
          showLineNumbers={showLineNumbers}
          theme={themeComponent[theme]}
          wrapLines={wrapLines}
          highlight={highlight}
          codeBlock
      />
      : codeStyle === 'copy' ?
          <CopyBlock
              language={Languages[language]}
              text={text}
              showLineNumbers={showLineNumbers}
              theme={themeComponent[theme]}
              wrapLines={wrapLines}
              highlight={highlight}
              codeBlock
          /> :
          <Code
              language={Languages[language]}
              text={text}
              theme={themeComponent[theme]}
          />
}

import { css } from '@emotion/react'

import {
  Dispatch,
  InputHTMLAttributes,
  SetStateAction,
  forwardRef,
  useMemo,
  useRef,
} from 'react'

import { styles as buttonStyles } from '@/components/atomic/Button'
import { Colors } from '@/styles/colors'
import { FORM_CONTROL_STYLE } from '@/styles/form'
import { Icons } from '@/styles/icons'
import { HTMLStyleProps } from '@/types'

import Icon from '../Icon'

export interface RadioProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'type'>,
    HTMLStyleProps {
  name?: string
  label?: React.ReactNode
  setValue?: Dispatch<SetStateAction<string>> | (() => void)
  isValid?: boolean
  type?: 'button'
}

export default forwardRef<HTMLInputElement, RadioProps>(function Radio(
  {
    name,
    value,
    label,
    defaultValue,
    setValue = () => undefined,
    onChange = () => undefined,
    isValid,
    disabled,
    checked,
    type,
    ...rest
  },
  ref
) {
  const inputEl = useRef<HTMLInputElement>(null)
  const error = useMemo(() => !isValid && isValid !== undefined, [isValid])
  const isButton = type === 'button'

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    onChange(e)
  }

  const icon: keyof typeof Icons = useMemo(() => {
    if (checked) {
      return 'radioChecked'
    }
    return 'radioUnchecked'
  }, [checked])

  const labelStyle = isButton ? styles.button : styles.label

  return (
    <div css={styles.container({ type })}>
      <label css={labelStyle({ error, disabled, checked })}>
        <input
          type='radio'
          ref={ref ?? inputEl}
          name={name}
          value={value}
          checked={checked}
          onChange={handleChange}
          disabled={disabled}
          {...rest}
        />
        {!isButton && <Icon icon={icon} css={styles.icon(error)} aria-hidden />}
        {label && <span css={styles.text(type)}>{label}</span>}
      </label>
    </div>
  )
})

const { color } = FORM_CONTROL_STYLE

const styles = {
  container: ({ type }: { type?: 'button' }) => css`
    display: inline-block;
    vertical-align: middle;
    margin-right: ${type === 'button' ? 10 : 22}px;
    &:last-child {
      margin-right: 0;
    }
  `,
  icon: (error?: boolean) => css`
    ${error &&
    css`
      color: ${Colors.RED};
    `}
  `,
  label: ({
    error,
    disabled,
  }: {
    checked?: boolean
    error: boolean
    disabled?: boolean
  }) => css`
    box-sizing: border-box;
    display: flex;
    align-items: center;
    min-height: 30px;

    input {
      position: absolute;
      left: -9999px;
    }

    ${error &&
    css`
      color: ${color.error};
    `}

    ${disabled &&
    css`
      color: ${color.disabled};
    `}
  `,
  button: ({
    checked,
    disabled,
  }: {
    checked?: boolean
    disabled?: boolean
  }) => css`
    ${buttonStyles.button({ primary: checked, size: 32 })}
    padding-left: 15px;
    padding-right: 15px;
    cursor: pointer;

    min-height: 38px;
    min-width: 70px;

    input {
      position: absolute;
      left: -9999px;
    }

    ${disabled &&
    css`
      color: ${color.disabled};
    `}
  `,
  text: (type?: 'button') => css`
    padding-left: 5px;
    padding-top: 2px;
    ${type === 'button' &&
    css`
      padding-left: 0;
      padding-top: 0;
      text-align: center;
    `}
  `,
}

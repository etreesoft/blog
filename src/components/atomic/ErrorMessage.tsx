import { css } from '@emotion/react'

import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
import { HTMLStyleProps } from '@/types'

export interface ErrorMessageProps extends HTMLStyleProps {
  message: string
}

export default function ErrorMessage({ message, ...rest }: ErrorMessageProps) {
  if (!message) {
    return null
  }

  return (
    <div {...rest}>
      <p css={styles.message}>{message}</p>
    </div>
  )
}

const styles = {
  message: css`
    ${Fonts.REGULAR_14};
    color: ${Colors.RED};
  `,
}

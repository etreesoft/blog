import { css } from '@emotion/react'

import React, {
  Dispatch,
  InputHTMLAttributes,
  SetStateAction,
  forwardRef,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react'

import { FORM_CONTROL, FORM_CONTROL_STYLE } from '@/styles/form'
import { HTMLStyleProps } from '@/types'

import ErrorMessage from './ErrorMessage'

type SizeType = 'small' | 'medium'
type AlignType = 'left' | 'right' | 'center'
export interface InputProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'size'>,
    HTMLStyleProps {
  type?: 'text' | 'password' | 'email' | 'number' | 'tel' | 'url'
  size?: SizeType
  setValue?: Dispatch<SetStateAction<string>> | (() => void)
  isValid?: boolean
  errorMessage?: string
  autoFocus?: boolean
  fullWidth?: boolean
  align?: AlignType
  extra?: React.ReactNode
}

export default forwardRef<HTMLInputElement, InputProps>(function Input(
  {
    type = 'text',
    size = 'medium',
    value,
    defaultValue,
    setValue = () => undefined,
    onChange = () => undefined,
    isValid,
    errorMessage = '',
    autoFocus = false,
    fullWidth = false,
    align = 'left',
    extra,
    style,
    className,
    ...rest
  },
  ref
) {
  const inputEl = useRef<HTMLInputElement>(null)

  useEffect(() => {
    if (autoFocus && inputEl.current) {
      inputEl.current.focus()
    }
  }, [autoFocus])

  const showError: boolean = useMemo(
    () => !isValid && isValid !== undefined,
    [isValid]
  )

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const value = e?.target?.value
      setValue(value)
      onChange(e)
    },
    [onChange, setValue]
  )

  const styleProps = { className, style }

  return (
    <>
      <div css={styles.container(fullWidth)} {...styleProps} data-form>
        <input
          type={type}
          ref={ref ?? inputEl}
          value={value}
          defaultValue={defaultValue}
          onChange={handleChange}
          css={styles.input({ size, align, showError })}
          {...rest}
        />
        {extra}
      </div>

      {showError && (
        <ErrorMessage message={errorMessage} css={styles.errorMessage} />
      )}
    </>
  )
})

const { borderColor, color } = FORM_CONTROL_STYLE

export const styles = {
  container: (fullWidth?: boolean) => css`
    position: relative;
    display: inline-block;
    width: 200px;
    max-width: 100%;
    vertical-align: middle;
    margin-right: 10px;
    line-height: 0;
    ${fullWidth &&
    css`
      width: 100%;
      margin-right: 0;
    `}
  `,
  input: ({
    size,
    align,
    showError,
  }: {
    size?: SizeType
    align?: AlignType
    showError?: boolean
  }) =>
    css`
      ${FORM_CONTROL};
      border-color: ${borderColor.primary};
      color: ${color.primary};
      text-align: ${align};

      ${size === 'small' &&
      css`
        min-height: 32px;
        padding-left: 8px;
        padding-right: 8px;
      `}

      ${showError &&
      css`
        &,
        &:read-only {
          border-color: ${borderColor.error};
          color: ${color.error};
        }
      `}
    `,
  errorMessage: css`
    margin-top: 4px;
  `,
}

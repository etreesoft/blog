import { css } from '@emotion/react'

import {
  PropsWithChildren,
  useCallback,
  useLayoutEffect,
  useRef,
  useState,
} from 'react'

import { Colors, ColorsKey } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
import { HTMLStyleProps } from '@/types'

import Icon from './Icon'

export interface TooltipProps extends HTMLStyleProps {
  label: string
  iconColor?: ColorsKey
}

type PositionX = 'left' | 'right'
type PositionY = 'top' | 'bottom'

function Tooltip({
  label,
  iconColor = 'BLACK_50',
  children,
  ...rest
}: PropsWithChildren<TooltipProps>) {
  const [x, setX] = useState<PositionX>('right')
  const [y, setY] = useState<PositionY>('bottom')
  const [size, setSize] = useState({ width: 0, height: 0 })

  const targetRef = useRef<HTMLDivElement>(null)
  const tooltipRef = useRef<HTMLDivElement>(null)

  const openTooltip = useCallback(() => {
    if (!tooltipRef.current || !targetRef.current) {
      return
    }

    const { innerHeight, innerWidth } = window
    const { height, width } = size
    const { right, bottom } = targetRef.current.getBoundingClientRect()

    const x = right + width > innerWidth ? 'left' : 'right'
    const y = bottom + height > innerHeight ? 'top' : 'bottom'

    setX(x)
    setY(y)

    /** Tooltip Position 확정 이후에 display되도록 setTimeout 적용  */
    setTimeout(() => {
      if (tooltipRef.current) {
        tooltipRef.current.style.display = 'block'
      }
    })
  }, [size])

  const closeTooltip = () => {
    if (!tooltipRef.current) {
      return
    }

    tooltipRef.current.style.display = 'none'
  }

  useLayoutEffect(() => {
    /**
     * [Tooltip 사이즈를 저장한 후, 'display: none' 처리하는 이유]
     * Tooltip이 펼쳐질 방향을 구하기 위해서는 Tooltip의 사이즈가 필요함
     * 동시에 Tooltip은 MouseEnter 이벤트 시에만 노출되어야 하기에 숨김 처리가 필요함
     * 'visibility: hidden'은 사이즈를 얻을 수 있으나 layout 영역을 차지하여 불필요한 scroll을 발생시킴
     * 'display: none'은 layout 영역을 차지하지 않아 사이즈를 얻을 수 없음
     * 따라서 render 전에 Tooltip 사이즈를 저장한 후 'display: none'을 적용하여 숨김 처리함
     */

    if (!tooltipRef.current) {
      return
    }

    const { width, height } = tooltipRef.current.getBoundingClientRect()
    setSize({ width, height })
    tooltipRef.current.style.display = 'none'
  }, [])

  return (
    <div
      {...rest}
      ref={targetRef}
      onMouseEnter={openTooltip}
      onMouseLeave={closeTooltip}
      css={styles.target}
      aria-describedby={`${label}-tooltip`}
    >
      <div css={styles.label}>{label}</div>
      <div css={styles.tooltip}>
        <Icon icon={'tooltipQuestionMark'} css={styles.icon(iconColor)} />
        <div
          ref={tooltipRef}
          css={styles.content({ x, y })}
          id={`${label}-tooltip`}
          role={'tooltip'}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

const styles = {
  target: css`
    display: inline-flex;
    align-items: center;
    padding-top: 2px;
    cursor: default;
  `,
  label: css``,
  tooltip: css`
    position: relative;
    top: -1px;
    margin-left: 4px;
    display: flex;
    align-items: center;
  `,
  icon: (iconColor: ColorsKey) => css`
    color: ${Colors[iconColor]};
    /* UX를 위한 MouseEvent 영역 확대 */
    ::before {
      content: '';
      position: absolute;
      border-radius: 50%;
      width: 180%;
      height: 180%;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      z-index: -1;
    }
  `,
  content: ({ x, y }: { x: PositionX; y: PositionY }) => css`
    position: absolute;
    width: max-content;
    /* 폰트 어센더/디센더 대응 (대응 전 padding: 10px 14px)*/
    padding: 11px 14px 9px 14px;
    margin: 4px 0px;
    background-color: white;
    border: 1px solid ${Colors.BLACK};
    border-radius: 4px;
    z-index: 20;
    color: ${Colors.BLACK};
    ${Fonts.REGULAR_12};

    ${x === 'left' ? 'right' : 'left'}: 6px;
    ${y === 'bottom' ? 'top' : 'bottom'}: 100%;

    /* TODO: Tooltip max-width 정책 생길 경우 변경사항 적용 필요함 */
    max-width: 300px;
    white-space: pre-line;
    word-break: break-all;
  `,
}

export default Tooltip

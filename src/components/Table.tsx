import { css } from '@emotion/react'
import { Table as TableBase } from 'antd'
import type { ColumnGroupType, ColumnType, TableProps } from 'antd/es/table'
import _ from 'lodash'

import { useMemo } from 'react'

import { Colors } from '@/styles/colors'

import Icon from './Icon'
import { PaginationContainer } from './Pagination'

type Column<Model = unknown> = ColumnType<Model> & {
  cellAlign?: 'left' | 'center' | 'right'
}
interface ColumnGroup<Model> extends Omit<Column<Model>, 'dataIndex'> {
  children: Columns<Model>
}
export type Columns<Model = unknown> = (ColumnGroup<Model> | Column<Model>)[]

type RowHeightType = 'default' | 'dense' | 'headerDense' | 'bodyDense'
type BorderType = 'default' | 'header' | 'body' | 'all'

export interface TableComponentProps<Model>
  extends Omit<TableProps<Model>, 'pagination' | 'columns' | 'bordered'> {
  columns: Columns<Model>
  rowSelectionType?: 'checkbox' | 'radio'
  rowType?: RowHeightType
  borderType?: BorderType
  empty?: { text?: string; height?: number }
}

/**
 * @see https://ant.design/components/table
 */
export default function Table<Model extends object>({
  columns,
  dataSource,
  rowSelection,
  rowSelectionType = 'checkbox',
  rowType = 'default',
  borderType = 'default',
  empty = { text: '검색결과가 없습니다.', height: 250 },
  className,
  style,
  ...rest
}: TableComponentProps<Model>) {
  // columns - 커스텀 props 주입
  const parseColumns = useMemo(
    () =>
      columns?.map((col) => {
        const makeProps = (col: Column<Model>) => {
          const { align, className = '', cellAlign, ...rest } = col
          const classes = className.split(' ')

          return {
            align: align || 'center',
            className: [
              ...classes,
              ...(cellAlign ? [`cell-align-${cellAlign}`] : []),
            ].join(' '),
            ...rest,
          }
        }

        const isColumnGroup = _.has(col, 'children')
        if (isColumnGroup) {
          return {
            ...makeProps(col),
            children: (col as ColumnGroupType<Model>).children.map((child) =>
              makeProps(child)
            ),
          }
        }

        return makeProps(col)
      }),
    [columns]
  )

  // row(data) default props 주입
  const parseDataSource = useMemo(
    () => dataSource?.map((item, i) => ({ ...item, key: i })),
    [dataSource]
  )

  const containerProps = {
    className,
    style,
    'data-table-row-type': rowType,
    'data-table-border-type': borderType,
  }

  return (
    <div css={styles.container} data-table {...containerProps}>
      <TableBase<Model>
        columns={parseColumns}
        dataSource={parseDataSource}
        locale={{
          emptyText: (
            <span css={styles.empty(empty.height)}>
              <Icon icon='tableEmpty' />
              {empty.text}
            </span>
          ),
        }}
        rowSelection={
          rowSelection && {
            type: rowSelectionType,
            ...rowSelection,
          }
        }
        pagination={false}
        {...rest}
      />
    </div>
  )
}

const styles = {
  container: css`
    margin-bottom: 40px;
    border-top: 2px solid ${Colors.BLACK};
    border-bottom: 1px solid ${Colors.COOL_GRAY_10};

    & + ${PaginationContainer} {
      margin-top: -20px;
    }
  `,
  empty: (height?: number) => css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: ${height}px;
    color: ${Colors.COOL_GRAY_20};
    gap: 20px;
  `,
}

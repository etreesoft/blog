import { css } from '@emotion/react'

import { PropsWithChildren } from 'react'

import { HTMLStyleProps } from '@/types'
import { injectProps, makeCompoundComponent } from '@/utils/common'

type Spacing = 'small' | 'medium' | 'large' | 'none' | number
type SpacingType = Spacing | [Spacing, Spacing]

interface GridProps extends HTMLStyleProps {
  spacing?: SpacingType
  gap?: SpacingType
  align?: 'left' | 'right' | 'center' | 'justify'
}

function Grid({
  spacing = 'none',
  gap = 'small',
  align,
  children,
  className,
  style,
}: PropsWithChildren<GridProps>) {
  return (
    <div
      css={styles.container({ spacing, gap })}
      className={className}
      style={style}
    >
      {injectProps(children, ['Row', 'Col'], { align, gap }, 'child')}
    </div>
  )
}

type RowProps = Omit<GridProps, 'spacing'>
type ColProps = { flex?: 'grow' | 'auto' } & HTMLStyleProps

function Row({ children, gap, align }: PropsWithChildren<RowProps>) {
  return <div css={styles.row({ gap, align })}>{children}</div>
}

function Col({
  children,
  flex = 'grow',
  ...rest
}: PropsWithChildren<ColProps>) {
  return (
    <div css={styles.col({ flex })} {...rest}>
      {children}
    </div>
  )
}

export default makeCompoundComponent(Grid, { Row, Col })

const makeSpacing = (type?: SpacingType, target?: 'gap' | 'spacing') => {
  if (!type) {
    return ''
  }

  const spaceMap = { small: 20, medium: 40, large: 70, none: 0 }
  const spacing = Array.isArray(type) ? type : [type, type]

  const cssProp = target === 'gap' ? 'gap' : 'margin'
  const cssPropValue = spacing.map((space) => {
    if (typeof space === 'number') {
      return space
    }
    return spaceMap[space]
  })

  return cssProp === 'margin'
    ? `margin-top:${cssPropValue[0]}px; margin-bottom:${cssPropValue[1]}px`
    : `gap: ${cssPropValue[0]}px ${cssPropValue[1]}px`
}

const styles = {
  container: ({ spacing }: Partial<GridProps>) => css`
    ${makeSpacing(spacing, 'spacing')};
  `,
  row: ({ gap, align }: Partial<GridProps>) => css`
    display: flex;
    flex-wrap: wrap;
    ${makeSpacing(gap, 'gap')};
    ${makeSpacing(gap, 'spacing')};

    &:first-of-type {
      margin-top: 0;
    }

    &:last-of-type {
      margin-bottom: 0;
    }

    &:first-of-type:last-of-type {
      margin-top: 0;
      margin-bottom: 0;
    }

    ${align &&
    css`
      justify-content: ${align === 'justify' ? 'space-between' : align};
      & > div {
        flex-grow: 0;
      }
    `}
  `,
  col: ({ flex }: ColProps) => css`
    flex: 1;
    align-self: center;

    ${flex === 'auto' &&
    css`
      flex-grow: 0;
      flex-basis: fit-content;
    `}
    ${flex === 'grow' &&
    css`
      flex-grow: 1;
    `}
  `,
}

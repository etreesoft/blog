import { css } from '@emotion/react'

import { PropsWithChildren } from 'react'

import { Fonts } from '@/styles/fonts'

interface DefaultLayoutProps {
  title: string
}

export default function DefaultLayout({
  title,
  children,
}: PropsWithChildren<DefaultLayoutProps>) {
  return (
    <div css={styles.container}>
      <div css={styles.header}>
        <h1 css={styles.title}>{title}</h1>
      </div>
      <div css={styles.contents}>{children}</div>
    </div>
  )
}

const styles = {
  container: css`
    height: 100%;
    box-sizing: border-box;
    padding: 60px 78px;
  `,
  header: css`
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    min-height: 56px;
    margin-bottom: 40px;
  `,
  title: css`
    ${Fonts.BOLD_24};
    margin-bottom: 0;
  `,
  contents: css`
    flex-grow: 1;
  `,
}

import { css } from '@emotion/react'

import {
  MouseEvent,
  PropsWithChildren,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react'

import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
import { HTMLStyleProps } from '@/types'
import { injectProps, makeCompoundComponent } from '@/utils/common'

import Icon from './Icon'

export interface AccordionProps extends HTMLStyleProps {
  toggle?: boolean
}

function Accordion({
  children,
  toggle,
  ...rest
}: PropsWithChildren<AccordionProps>) {
  const accordionRef = useRef<HTMLDivElement>(null)

  const handleClick = useCallback(
    (e: MouseEvent<HTMLDListElement>) => {
      const target = e.currentTarget.parentElement
      const isOpen = target?.getAttribute('data-open') === 'true'

      target?.setAttribute('data-open', isOpen ? 'false' : 'true')

      if (toggle) {
        return
      }

      /** Close Other Items on Accordion Mode */
      Array.from(accordionRef.current?.querySelectorAll('dl') || []).forEach(
        (el) => {
          const shouldClose =
            el !== target && el.getAttribute('data-fixed') !== 'true'
          shouldClose && el.setAttribute('data-open', 'false')
        }
      )
    },
    [toggle]
  )

  return (
    <div ref={accordionRef} css={styles.accordion} {...rest}>
      {injectProps(children, ['Item'], { handleClick })}
    </div>
  )
}

export interface AccordionItemProps extends HTMLStyleProps {
  open?: boolean
  fixed?: boolean
  handleClick?: (e: MouseEvent<HTMLDListElement>) => void
}

function Item({
  children,
  open = false,
  fixed = false,
  handleClick,
  ...rest
}: PropsWithChildren<AccordionItemProps>) {
  const itemRef = useRef<HTMLDListElement>(null)

  useEffect(() => {
    open && itemRef.current?.setAttribute('data-open', 'true')
    fixed && itemRef.current?.setAttribute('data-fixed', 'true')
  }, [fixed, open])

  return (
    <dl ref={itemRef} css={styles.item} {...rest}>
      {injectProps(children, ['Header'], {
        handleClick,
        fixed,
      })}
    </dl>
  )
}

function Header({
  children,
  handleClick,
  fixed,
  ...rest
}: PropsWithChildren<AccordionItemProps>) {
  return (
    <dt
      onClick={(e: MouseEvent<HTMLDListElement>) => !fixed && handleClick?.(e)}
      css={styles.header}
      {...rest}
    >
      <button type={'button'}>{children}</button>
      <Icon icon={fixed ? 'minus' : 'dropdownArrow'} />
    </dt>
  )
}

function Body({ children, ...rest }: PropsWithChildren<AccordionItemProps>) {
  const [bodyHeight, setBodyHeight] = useState(0)
  const bodyRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setBodyHeight(bodyRef.current?.clientHeight || 0)
  }, [])

  return (
    <dd ref={bodyRef} css={styles.bodyWrapper} {...rest}>
      <div css={styles.body({ bodyHeight })}>{children}</div>
    </dd>
  )
}

export default makeCompoundComponent(Accordion, {
  Item: makeCompoundComponent(Item, { Header, Body }),
})

const styles = {
  accordion: css`
    padding: 20px 0;
    width: 100%;
    display: flex;
    flex-direction: column;
    gap: 30px;
  `,

  item: css`
    i {
      transition: all 0.2s ease-in-out;
    }

    &[data-open='true'],
    &[data-fixed='true'] {
      i {
        transform: rotate(180deg);
      }

      dd > div {
        margin-top: 0px;
        opacity: 1;
      }
    }

    &[data-fixed='true'] {
      dt {
        cursor: default;
      }
    }
  `,

  header: css`
    list-style-type: none;
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    padding-bottom: 18px;
    border-bottom: 2px solid ${Colors.BLACK};
    cursor: pointer;
    ${Fonts.BOLD_16}
  `,

  bodyWrapper: css`
    overflow: hidden;
  `,

  body: ({ bodyHeight }: { bodyHeight: number }) => css`
    transition: all 0.2s ease-in-out;
    margin-top: -${bodyHeight}px;
    opacity: 0;
    ${Fonts.REGULAR_14};
  `,
}

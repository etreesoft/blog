import { css } from '@emotion/react'

import {
  HTMLAttributes,
  PropsWithChildren,
  TableHTMLAttributes,
  TdHTMLAttributes,
  ThHTMLAttributes,
} from 'react'

import { Colors } from '@/styles/colors'
import * as Styles from '@/styles/description'
import { HTMLStyleProps } from '@/types'
import { makeCompoundComponent } from '@/utils/common'

type TableProps = PropsWithChildren<
  Omit<TableHTMLAttributes<HTMLTableElement>, 'border'>
>

export type AlignType = 'left' | 'center' | 'right'
export type RowHeightType = 'default' | 'dense' | 'headerDense' | 'bodyDense'
export type DescriptionTypes = 'default' | 'inner' | 'transparent' | 'search'

export interface StyleProps extends HTMLStyleProps {
  type?: DescriptionTypes
  rowType?: RowHeightType
  cellType?: 'form'
  align?: AlignType
  inner?: boolean
  border?: boolean
  bottomBorder?: boolean
}

export interface DescriptionProps extends TableProps, StyleProps {
  type?: DescriptionTypes
  rowType?: RowHeightType
  colWidth?: Array<string | number>
  align?: AlignType
}

function Description({
  type = 'default',
  colWidth,
  align,
  bottomBorder,
  rowType = 'default',
  border = false,
  children,
  className,
  style,
}: PropsWithChildren<DescriptionProps>) {
  const styleProps: StyleProps = {
    type,
    align: align ?? (type === 'inner' ? 'center' : 'left'),
    inner: type === 'inner',
    bottomBorder,
    border,
    rowType,
  }

  const styleMap = {
    default: Styles.defaultTableStyle,
    inner: Styles.innerTableStyle,
    transparent: Styles.transparentTableStyle,
    search: Styles.searchTableStyle,
  }

  return (
    <div css={styles.container(styleProps)} className={className} style={style}>
      <table css={styleMap[type](styleProps)}>
        {colWidth && (
          <colgroup>
            {colWidth.map((width, index) => (
              <col key={index} style={{ width }} />
            ))}
          </colgroup>
        )}
        {children}
      </table>
    </div>
  )
}

type SectionProps = PropsWithChildren<HTMLAttributes<HTMLTableSectionElement>>
interface TrProps
  extends PropsWithChildren<HTMLAttributes<HTMLTableRowElement>> {
  emphasisBorderBottom?: boolean
  rowType?: RowHeightType
}
interface ThProps
  extends PropsWithChildren<
      Pick<ThHTMLAttributes<HTMLTableCellElement>, 'rowSpan' | 'colSpan'>
    >,
    HTMLStyleProps {
  type?: 'form'
  required?: boolean
  align?: AlignType
}

interface TdProps
  extends PropsWithChildren<
      Pick<TdHTMLAttributes<HTMLTableCellElement>, 'rowSpan' | 'colSpan'>
    >,
    HTMLStyleProps {
  align?: AlignType
}

const Partial = {
  Thead: ({
    size = 'medium',
    ...rest
  }: SectionProps & Styles.CellStyleProps) => (
    <thead {...rest} data-size={size} />
  ),
  Tbody: (props: SectionProps) => (
    <>
      <tbody {...props} />
    </>
  ),
  Row: ({ emphasisBorderBottom, rowType, ...rest }: TrProps) => (
    <>
      <tr {...rest} css={Styles.rowStyle({ emphasisBorderBottom, rowType })} />
    </>
  ),
  Th: ({ children, type, align, required, ...rest }: ThProps) => {
    return (
      <th {...rest}>
        <div
          css={Styles.cellStyle({ parent: 'th', cellType: type, align })}
          data-cell-text
        >
          <div>
            {children}
            {required && <i css={styles.requiredBadge} />}
          </div>
        </div>
      </th>
    )
  },
  Td: ({
    children,
    hasInner,
    align,
    ...rest
  }: TdProps & Styles.CellStyleProps) => (
    <td {...rest}>
      <div
        css={Styles.cellStyle({ parent: 'td', hasInner, align })}
        data-cell-text
      >
        <div>{children}</div>
      </div>
    </td>
  ),
}

export default makeCompoundComponent(Description, { ...Partial })

const styles = {
  container: ({ type, inner }: StyleProps) => css`
    margin-bottom: 70px;

    ${(type === 'search' || inner) &&
    css`
      margin-bottom: 0;
    `}
  `,
  requiredBadge: css`
    position: relative;
    top: -1px;
    display: inline-block;
    width: 5px;
    height: 5px;
    margin-left: 7px;
    border-radius: 50%;
    vertical-align: middle;
    background-color: ${Colors.RED};
  `,
}

import { SerializedStyles, css } from '@emotion/react'
import styled from '@emotion/styled/macro'

import { HTMLAttributes, PropsWithChildren } from 'react'

import { Colors, ColorsKey } from '@/styles/colors'
import { Fonts, FontsKey } from '@/styles/fonts'
import { HTMLStyleProps } from '@/types'
import { makeCompoundComponent } from '@/utils/common'

interface CommonProps extends HTMLStyleProps {
  gap?: 14 | 20
  extra?: React.ReactNode
  extraRight?: React.ReactNode
}

function TypographyContainer({
  gap = 20,
  extra,
  extraRight,
  children,
  ...rest
}: PropsWithChildren<CommonProps>) {
  return (
    <>
      <div css={styles.container({ gap })} {...rest}>
        {children}
        {extra && <div css={styles.extra()}>{extra}</div>}
        {extraRight && <div css={styles.extra(true)}>{extraRight}</div>}
      </div>
    </>
  )
}

export interface TitleProps
  extends CommonProps,
    HTMLAttributes<HTMLHeadingElement> {
  level?: 1 | 2 | 3 | 4 | 5 | 6
  subText?: string
  subTextPlacement?: 'right' | 'bottom'
}

function Title({
  level = 1,
  subText,
  subTextPlacement = 'right',
  extra,
  extraRight,
  gap = 20,
  children,
  ...rest
}: TitleProps) {
  return (
    <TypographyContainer
      extra={extra}
      extraRight={extraRight}
      gap={gap}
      {...rest}
    >
      <div css={styles.heading({ subTextPlacement })}>
        <Heading as={`h${level}`} level={level}>
          {children}
        </Heading>
        {subText && <p css={styles.subText({ subTextPlacement })}>{subText}</p>}
      </div>
    </TypographyContainer>
  )
}

export interface TextProps extends CommonProps, HTMLAttributes<HTMLDivElement> {
  type?: FontsKey
  color?: ColorsKey
  extra?: React.ReactNode
  extraRight?: React.ReactNode
  align?: 'left' | 'center' | 'right'
}

function Text({
  type = 'REGULAR_16',
  color = 'BLACK',
  extra,
  extraRight,
  gap,
  align = 'left',
  children,
  ...rest
}: TextProps) {
  return (
    <TypographyContainer
      extra={extra}
      extraRight={extraRight}
      gap={gap}
      {...rest}
    >
      <div css={styles.text({ type, color, align })}>{children}</div>
    </TypographyContainer>
  )
}

export default makeCompoundComponent({}, { Title, Text })

const titleSizeMap: {
  [key: number]: SerializedStyles
} = {
  1: Fonts.BOLD_20,
  2: Fonts.BOLD_18,
  3: Fonts.BOLD_16,
  4: Fonts.REGULAR_16,
  5: Fonts.REGULAR_14,
  6: Fonts.REGULAR_12,
}

const Heading = styled.h1<TitleProps>`
  margin-bottom: 0;

  ${({ level }) => css`
    ${titleSizeMap[level ?? 1]};
  `}
`

const styles = {
  container: ({ gap }: { gap: 14 | 20 }) => css`
    display: flex;
    align-items: center;
    margin-bottom: ${gap}px;
  `,
  heading: ({ subTextPlacement }: TitleProps) => css`
    display: flex;
    align-items: center;

    ${subTextPlacement === 'bottom' &&
    css`
      flex-direction: column;
      align-items: flex-start;
      justify-content: center;
    `}
  `,
  subText: ({ subTextPlacement }: TitleProps) => css`
    ${Fonts.REGULAR_14};
    color: ${Colors.COOL_GRAY_30};
    padding-top: 2px;
    padding-left: 10px;

    ${subTextPlacement === 'bottom' &&
    css`
      padding-left: 0;
      padding-top: 10px;
    `}
  `,
  text: ({
    type,
    color,
    align,
  }: {
    type: FontsKey
    color: ColorsKey
    align: 'left' | 'center' | 'right'
  }) => css`
    ${Fonts[type]};
    padding-top: 4px;
    text-align: ${align};
    color: ${Colors[color]};
  `,
  extra: (right?: boolean) => css`
    padding-left: 20px;

    > * + * {
      margin-left: 10px;
    }
    > button + button {
      margin-left: 6px;
    }
    > :last-child {
      margin-right: 0;
    }

    ${right &&
    css`
      margin-left: auto;
      align-self: flex-end;
    `}
  `,
}

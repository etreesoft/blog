export const PATH_PREFIX = {
  sample: '/sample', // 샘플
  test: '/test', // test
} as const

// route path
export const sample = `${PATH_PREFIX.sample}` as const
export const test = `${PATH_PREFIX.test}` as const

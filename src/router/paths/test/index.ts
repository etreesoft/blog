import { test } from '@/server/routes/api'

import SampleTestList from '@/pages/test/List'
import SampleTestDetail_1 from '@/pages/test/Detail_1'
import SampleTestDetail_2 from '@/pages/test/Detail_2'
import { RouteItem } from '@/router'

export const RouteTestDetail_1: RouteItem= {
  path: `${test}/detail_1`,
  component: SampleTestDetail_1,
  page: '테스트 상세1'
}

export const RouteTestDetail_2: RouteItem= {
  path: `${test}/detail_2`,
  component: SampleTestDetail_2,
  page: '테스트 상세2'
}
export const RouteTestList: RouteItem = {
  path: `${test}`,
  component: SampleTestList,
  page: '테스트 목록',
  type: 'list',
}

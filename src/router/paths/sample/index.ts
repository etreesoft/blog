import { sample } from '@/server/routes/api'

import SampleSampleList from '@/pages/sample/List'
import SampleSampleDetail_1 from '@/pages/sample/Detail_1'
import SampleSampleDetail_2 from '@/pages/sample/Detail_2'
import { RouteItem } from '@/router'

export const RouteSampleDetail_1: RouteItem= {
  path: `${sample}/detail_1`,
  component: SampleSampleDetail_1,
  page: '샘플 상세1'
}

export const RouteSampleDetail_2: RouteItem= {
  path: `${sample}/detail_2`,
  component: SampleSampleDetail_2,
  page: '샘플 상세2'
}

export const RouteSampleList: RouteItem = {
  path: `${sample}`,
  component: SampleSampleList,
  page: '샘플 목록',
  type: 'list',
}

import { RouteProps } from 'react-router'
import * as sampleRoutes from '@/router/paths/sample'
import * as testRoutes from '@/router/paths/test'
import Main from '@/pages/Main'
import NoMatch from '@/pages/NoMatch'

export type RouteItem = RouteProps & {
  component: React.ComponentType
  path: string
  routes?: RouteItem[]
  page?: string
  type?: string
}

export const MainRoute: RouteItem = {
  path: '/',
  component: Main,
  type: 'main' 
}

const routes: RouteItem[] = Object.values(
  Object.assign(
    {},
    { Main: MainRoute },
    sampleRoutes,
    testRoutes,
    { NoMatch: { path: '*', component: NoMatch, type: 'noMatch' } }
  )
)

export default routes;

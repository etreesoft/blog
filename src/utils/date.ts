import { addDays as addDaysDateFns } from 'date-fns'
import { OptionsWithTZ, format, toDate } from 'date-fns-tz'

export const dateFormat = (
  date: string | number | Date | undefined,
  formatString: string = 'date',
  fallback = '-',
  options?: Omit<OptionsWithTZ, 'locale'>
) => {
  if (!date) {
    return fallback
  }
  const dateInstance = toDate(date)

  if (isNaN(dateInstance.getTime())) {
    return fallback
  }

  return format(
    dateInstance,
    formatString,
    {
      ...options,
    }
  )
}

export enum Days {
  SUN = 'SUN',
  MON = 'MON',
  TUE = 'TUE',
  WED = 'WED',
  THU = 'THU',
  FRI = 'FRI',
  SAT = 'SAT',
}

export enum DaysKR {
  SUN = '일',
  MON = '월',
  TUE = '화',
  WED = '수',
  THU = '목',
  FRI = '금',
  SAT = '토',
}

export const days = Object.values(Days)

export const getToday = () => dateFormat(new Date())

export function addDays(date: string, num: number): string
export function addDays(date: Date, num: number): Date
export function addDays(date: string | Date, num: number): string | Date {
  if (typeof date === 'string') {
    return dateFormat(addDaysDateFns(new Date(date), num))
  }

  return addDaysDateFns(date, num)
}

/**
 * @description 남은 시간을 계산하는 함수입니다.
 * @param target 남은 시간을 계산할 대상 날짜
 * @returns 남은 시간 (ex. 1시간 30분) 혹은 '-' (남은 시간이 없을 경우)
 */
export const getRemainTime = (target: string | Date) => {
  let remain = ''
  const now = new Date()
  const targetDate = toDate(target)
  const diff = targetDate.getTime() - now.getTime()
  const hours = Math.floor(diff / (1000 * 60 * 60))
  const minutes = Math.floor((diff / 1000 / 60) % 60)

  if (hours > 0) {
    remain += `${hours}시간`
  }

  if (minutes > 0) {
    remain += `${minutes}분`
  }

  return remain.length ? remain : '-'
}

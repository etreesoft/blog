import { nanoid } from '@reduxjs/toolkit'

import { Children, FC, ReactNode, cloneElement, isValidElement } from 'react'

export const numberFormat = (
  number: number | string = '-',
  amountSymbol = false,
  fallback = '-'
): string => {
  if (number === null || isNaN(Number(number))) {
    return fallback
  }

  return (typeof number === 'string' ? Number(number) : number).toLocaleString(
    'ko',
    amountSymbol
      ? {
          style: 'currency',
          currency: 'KRW',
        }
      : {}
  )
}

export const makeUniqueKeys = <T>(data: T[] = [], key = 'id') => {
  return data.map((v) => ({ ...v, [key]: nanoid() }))
}

/**
 * Emotion styled 컴포넌트의 기본 설정 displayName: 'EmotionCssPropInternal'
 * displayName 식별을 위해 컴포넌트명으로 재설정해주는 함수
 *
 * @param parent 최상위 컴포넌트
 * @param children 참조(자식) 컴포넌트
 * @returns parent, children 을 합성한 복합 컴포넌트
 */
export const makeCompoundComponent = <
  T extends {},
  U extends { [key: string]: FC<never> }
>(
  parent: T,
  children: U
) => {
  Object.entries(children).forEach(
    ([displayName, child]) => (child.displayName = displayName)
  )

  return Object.assign(parent, children)
}

/**
 * Child Component에 props를 주입해주는 함수
 *
 * @param children 참조(자식) 컴포넌트
 * @param displayName 주입할 컴포넌트의 displayName 배열
 * @param props 주입할 props
 * @param overridePriority 주입할 props의 override 우선순위
 * @returns 주입된 props를 가진 children
 */
export const injectProps = (
  children: ReactNode,
  displayName: string[],
  props: unknown,
  overridePriority: 'inject' | 'child' = 'inject'
) => {
  return Children.map(children, (child) => {
    if (!isValidElement(child)) {
      return child
    }

    const { type } = child
    const isTarget =
      typeof type !== 'string' &&
      displayName.includes((type as { displayName?: string }).displayName || '')

    if (isTarget) {
      const injectProps = typeof props === 'object' ? props : {}
      const mergeProps =
        overridePriority === 'inject'
          ? { ...child.props, ...injectProps }
          : { ...injectProps, ...child.props }

      return cloneElement(child, mergeProps)
    }

    return child
  })
}

/**
 * 앞자리에 0을 붙여 자릿수를 일치시킬때 사용하는 함수
 *
 * @param num 변환할 숫자
 * @param length 최종 길이
 * @returns 0을 붙인 문자열
 */
export const addLeadingZero = (num: number | string, length = 2) => {
  return String(num).padStart(length, '0')
}

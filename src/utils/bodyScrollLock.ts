import {
  BodyScrollOptions,
  disableBodyScroll as disable,
  enableBodyScroll as enable,
} from 'body-scroll-lock'

export { clearAllBodyScrollLocks } from 'body-scroll-lock'

export const disableBodyScroll = (
  targetElement: HTMLElement | Element | null,
  options?: BodyScrollOptions
): void => {
  const { scrollY, scrollX } = window
  const { dataset, clientWidth = 1200 } = document.body

  window.scrollTo(0, 0)
  dataset.bodyScrollPosX = String(scrollX)
  dataset.bodyScrollPosY = String(scrollY)

  document.body.style.cssText = `
      position: fixed;
      min-width: ${clientWidth}px;
      margin-top: -${scrollY}px;
      margin-left: -${scrollX}px;
    `
  if (targetElement) {
    disable(targetElement, options)
  }
}

export const enableBodyScroll = (
  targetElement: HTMLElement | Element | null
): void => {
  const deleteCssProps = ['position', 'min-width', 'margin-top', 'margin-left']
  deleteCssProps.forEach((property) => {
    document.body.style.removeProperty(property)
  })

  const { scrollY, scrollX } = window
  const { dataset } = document.body

  window.scrollTo(
    Number(dataset.bodyScrollPosX) || scrollX,
    Number(dataset.bodyScrollPosY) || scrollY
  )
  if (targetElement) {
    enable(targetElement)
  }

  delete dataset.bodyScrollPosY
  delete dataset.bodyScrollPosX
}

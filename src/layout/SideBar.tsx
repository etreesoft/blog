import React from "react"
import { css } from '@emotion/react'
import routes, {RouteItem} from "@/router";
import {Link} from "react-router-dom";

type GroupCount = {
  [keys: string]: number
}

export default function SideBar() {
  const routeGroup = routes.reduce((group: GroupCount, route: RouteItem) => {
    const { path } = route
    const _path = '/' + path.split('/')[1]
    if (_path !== '' && _path !== undefined) {
      if (path.split('/').length > 2) {
        group[_path] = group[_path] ?? 0
        group[_path] = group[_path] + 1
      }
    }
    return group
  }, {})

  return (
      <aside css={styles.sidebar}>
        <ul css={styles.navUl}>
          {routes
              .map((route, i) => (
                  (route.type === 'list') ?
                      <li key={i} css={styles.navLi}>
                        <div>
                          <Link to={route.path}>
                            {route.page}({routeGroup[route.path]})
                          </Link>
                        </div>
                      </li>
                      : <template key={i}></template>
              ))
          }
        </ul>
      </aside>
  )
}

const styles = {
  sidebar: css`
    display: inline-block;
    position: absolute;
    left: 0;
    height: 100%;
    width: var(--aside-width);
    background-color: #dde0ab;
  `,
  navUl: css`
    list-style: none;
    text-align: left;
  `,
  navLi: css`
    padding: 10px 0 10px;
  `,
}

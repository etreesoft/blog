import React from "react"
import { css } from '@emotion/react'

import Header from '@/layout/Header';
import Footer from '@/layout/Footer';
import SideBar from "@/layout/SideBar";

interface layoutProperty {
  children : React.ReactNode
}

export default function Layout( { children }: layoutProperty ) {
  return (
    <div css={styles.layout}>
      <Header />
      <nav css={styles.nav}>
        <SideBar />
        <main css={styles.main}>
          { children }
        </main>
      </nav>
      <Footer />
    </div>
  )
}

const styles = {
  layout: css`
    padding-top: 80px;
  `,
  nav: css`
    position: relative;
    min-height: calc(100vh - 80px - 100px);
    max-width: var(--max-width);
    margin: auto;
  `,
  main: css`
    position: absolute;
    text-align: left;
    left: var(--aside-width);
  `,
}

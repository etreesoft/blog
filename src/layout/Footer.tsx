import { css } from '@emotion/react'

export default function Footer() {
  return (
    <footer css={styles.footer}>
      <div css={styles.contents}>
        <h2 css={styles.title}>
          footer
        </h2> 
      </div>
    </footer>
  )
}

const styles = {
  footer: css`
    height: 100px;
    margin-top: auto;
    background-color: #dde0dd;
  `,
  contents: css`
    width: 96%;
    max-width: 1100px;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
  `,
  title: css`
    font-weight: 600;
    font-size: 20px;
  `,
}

import { Link } from 'react-router-dom'
import { css } from '@emotion/react'
import routes, { MainRoute } from '@/router'

export default function Header() {
  return (
    <header css={style.header}>
      <div css={style.contents}>
        <div>
          <Link to={MainRoute.path}>
            <h2>Main</h2>
          </Link>
        </div>
        <div>
          <nav>
            <ul css={style.navUl}>
              {routes
                .map((route, i) => (
                  (route.type === 'list') ?
                  <li key={i} css={style.navLi}>
                    <div>
                      <Link to={route.path}>
                        {route.page}
                      </Link>
                    </div>
                  </li>
                      : <template key={i}></template>
                ))
              }
            </ul>
          </nav>
        </div>
      </div>
    </header>
  )
}

const style = {
  layout: css`
    padding-top: 80px;
  `,
  header: css`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 80px;
    background-color: #dde0ea;
  `,
  contents: css`
    display: flex;
    width: 96%;
    max-width: var(--header-max-width);
    height: 100%;
    margin: 0 auto;
    align-items: center;
    justify-content: space-between;
  `,
  navUl: css`
    display: flex;
    list-style: none;
  `,
  navLi: css`
    margin-left: 30px;
  `,
}

export const Colors = {
  BLACK: '#05141f',
  GREEN: '#5B7B2B',

  RED: '#EA0029',
  RED_5: 'rgba(234, 0, 41, 0.05)',
  RED_5_HEX: '#fdf2f5',

  BLACK_2: 'rgba(5, 20, 31, 0.02)',
  BLACK_20: 'rgba(5, 20, 31, 0.2)',
  BLACK_50: 'rgba(5, 20, 31, 0.5)',
  BLACK_70: 'rgba(5, 20, 31, 0.7)',

  GREEN_2: 'rgba(91, 123, 43, 0.2)',

  COOL_GRAY_10: '#F2F4F6',
  COOL_GRAY_20: '#CDD0D2',
  COOL_GRAY_30: '#B4B9BC',
  COOL_GRAY_50: '#82898F',
  COOL_GRAY_60: '#697279',

  COOL_GRAY_10_50: 'rgba(242, 244, 246, 0.5)',
  COOL_GRAY_10_50_HEX: '#f9fafb',
  COOL_GRAY_10_20: 'rgba(242, 244, 246, 0.2)',
  COOL_GRAY_20_30: 'rgba(205, 208, 210, 0.3)',
  COOL_GRAY_20_50: 'rgba(205, 208, 210, 0.5)',

  DIMMED: 'rgba(0,0,0,0.3)',
} as const

export type ColorsKey = keyof typeof Colors

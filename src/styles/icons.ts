const FormSize = {
  width: 14,
  height: 14,
}

export type IconType = {
  [key: string]: {
    source: string
    defaultSize: { width: number; height: number }
  }
}

/**
 * @description
 * 1. 부모 요소의 색상을 상속받거나, 색상을 변경할 수 있도록 'fill' , 'stroke' 속성의 값을 'currentColor' 로 설정.
 * 2. 아이콘의 defaultSize를 width: 20, height: 20, 등으로 Object property 로 설정.
 * 3. source는 빈줄없이 한줄로 작성.
 */
export const Icons: IconType = {
  minus: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M26.3748 15.0875H3.62476" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/></svg>',
    defaultSize: { ...FormSize },
  },
  plus: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.9999 3.72498V26.4625" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/><path d="M26.3748 15.0875H3.62476" stroke="currentColor" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/></svg>',
    defaultSize: { ...FormSize },
  },
  dragAndDropHandle: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 26 11" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="26" height="1" rx="0.5" fill="#05141F"/><rect y="10" width="26" height="1" rx="0.5" fill="#05141F"/></svg>',
    defaultSize: { width: 26, height: 26 },
  },
  close: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.29004 2.29004L21.71 21.71" stroke="currentColor" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round"/><path d="M21.71 2.29004L2.29004 21.71" stroke="currentColor" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round"/></svg>',
    defaultSize: { width: 24, height: 24 },
  },
  tooltipQuestionMark: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="9" cy="9" r="9" fill="currentColor"/><path d="M6.35821 4.424C7.02088 3.71467 7.84221 3.36 8.82221 3.36C9.36354 3.36 9.84888 3.472 10.2782 3.696C10.7075 3.92 11.0389 4.23267 11.2722 4.634C11.5149 5.03533 11.6362 5.49267 11.6362 6.006C11.6362 6.36067 11.5755 6.69667 11.4542 7.014C11.3422 7.33133 11.2022 7.616 11.0342 7.868C10.8755 8.11067 10.6329 8.45133 10.3062 8.89C9.96088 9.37533 9.70421 9.79533 9.53621 10.15C9.36821 10.5047 9.26554 10.878 9.22821 11.27H8.02421C8.03354 10.878 8.09888 10.514 8.22021 10.178C8.34154 9.842 8.47688 9.548 8.62621 9.296C8.78488 9.044 9.01821 8.68933 9.32621 8.232C9.67154 7.71867 9.91421 7.308 10.0542 7C10.2035 6.68267 10.2782 6.356 10.2782 6.02C10.2782 5.572 10.1335 5.194 9.84421 4.886C9.56421 4.578 9.18154 4.424 8.69621 4.424C8.19221 4.424 7.73488 4.578 7.32421 4.886C6.91354 5.194 6.59154 5.59067 6.35821 6.076V4.424ZM9.36821 14.21H7.87021V12.544H9.36821V14.21Z" fill="white"/></svg>',
    defaultSize: { ...FormSize },
  },
  radioUnchecked: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="7" cy="7" r="6.5" fill="white" stroke="currentColor"/></svg>',
    defaultSize: { ...FormSize },
  },
  radioChecked: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 14 14" fill="none"><circle cx="7" cy="7" r="5" fill="white" stroke="currentColor" stroke-width="4"/></svg>',
    defaultSize: { ...FormSize },
  },
  checkUnchecked: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 14 14" fill="none"><rect x="0.5" y="0.5" width="13" height="13" rx="1.5" fill="white" stroke="currentColor"/></svg>',
    defaultSize: { ...FormSize },
  },
  checkChecked: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 14 14" fill="none"><rect x="0.5" y="0.5" width="13" height="13" rx="1.5" fill="currentColor" stroke="currentColor"/><path d="M2.9165 6.70987L5.46859 10.5L11.0832 3.5" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { ...FormSize },
  },
  dropdownArrow: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 14 14" fill="none"><path d="M12.6759 4.16504L7.00005 9.83504L1.32422 4.16504" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { ...FormSize },
  },
  excel: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="none"><path d="M7.2971 15.125L9.71499 11.529L9.90276 11.2497L9.71474 10.9706L7.2981 7.3833H7.49212L9.58518 10.4957L9.99932 11.1115L10.4145 10.4964L12.5158 7.3833H12.702L10.2854 10.9706L10.0974 11.2497L10.2852 11.529L12.703 15.125H12.5158L10.4145 12.0119L9.99932 11.3968L9.58518 12.0126L7.49212 15.125H7.2971Z" fill="currentColor" stroke="currentColor"/><path d="M16.8917 1.90833H6.86674L3.1084 5.65832V18.0916H16.8917V1.90833Z" stroke="currentColor" stroke-linejoin="round"/><path d="M6.86674 1.90833V5.65832H3.1084" stroke="currentColor" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 20, height: 20 },
  },
  reset: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" fill="none"><path d="M9.625 4.95831H12.8333V1.74998" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/><path d="M12.113 4.42618C10.6914 1.60231 7.24987 0.465479 4.426 1.887C1.60213 3.30852 0.4653 6.75008 1.88682 9.57395C3.30834 12.3978 6.74991 13.5346 9.57377 12.1131C10.9673 11.4116 11.95 10.2182 12.4172 8.8498" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/></svg>',
    defaultSize: { width: 14, height: 14 },
  },
  arrowRight: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.57031 1.13507L8.43531 6.00007L3.57031 10.8651" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 12, height: 12 },
  },
  arrowPrev: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.8609 20.7295L2.13086 10.9995L11.8609 1.26953" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 8, height: 12 },
  },
  arrowNext: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.14062 1.26953L11.8706 10.9995L2.14062 20.7295" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 8, height: 12 },
  },
  paginationLast: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.40367 1.13486L6.26367 5.99986L1.40367 10.8599" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/><path d="M9.66602 10.8599L9.66601 1.13486" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 12, height: 12 },
  },
  exclamationMark: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 16 16" fill="none"><circle cx="8" cy="8" r="7.5" fill="currentColor"/><path d="M8.49022 10.27H7.52422L7.31422 2.57H8.70022L8.49022 10.27ZM8.75622 13.21H7.24422V11.544H8.75622V13.21Z" fill="white"/></svg>',
    defaultSize: { width: 15, height: 15 },
  },
  calendar: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 14 14" fill="none"><rect x="0.75" y="2.25" width="12.5" height="11" rx="1.25" stroke="currentColor" stroke-width="1.5"/><rect x="1" y="5.5" width="12" height="1.5" fill="currentColor"/><rect x="3.5" width="1.5" height="4.5" fill="currentColor"/><rect x="9" width="1.5" height="4.5" fill="currentColor"/></svg>',
    defaultSize: { width: 14, height: 14 },
  },
  leftAngle: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.8599 21.73L7.12988 12L16.8599 2.27002" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 24, height: 24 },
  },
  rightAngle: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.14014 2.27002L16.8701 12L7.14014 21.73" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 24, height: 24 },
  },
  rightAngleBold: {
    source:
      '<svg width="100%" height="100%" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.9751 0.945801L7.02926 4.99997L2.9751 9.05413" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg>',
    defaultSize: { width: 10, height: 10 },
  },
  tableEmpty: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 50 50" fill="none"><circle cx="25" cy="25" r="24.25" fill="white" stroke="currentColor" stroke-width="1.5"/><path d="M25.9107 29.93H24.1167L23.7267 15.63H26.3007L25.9107 29.93ZM26.4047 35.39H23.5967V32.296H26.4047V35.39Z" fill="currentColor"/></svg>',
    defaultSize: { width: 50, height: 50 },
  },
  loadingCircle: {
    source:
      '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"><path fill="currentColor" d="M10.14,1.16a11,11,0,0,0-9,8.92A1.59,1.59,0,0,0,2.46,12,1.52,1.52,0,0,0,4.11,10.7a8,8,0,0,1,6.66-6.61A1.42,1.42,0,0,0,12,2.69h0A1.57,1.57,0,0,0,10.14,1.16Z" /></svg>',
    defaultSize: { width: 24, height: 24 },
  },
} as const

/**
 * @description SVG Icon을 css background-image로 사용할 수 있게 변환해주는 함수
 * @usage ${makeIconsBackground('checkUnchecked', Colors.BLACK)}
 */
export const makeIconsBackground = (
  icon: keyof typeof Icons,
  color: string
) => {
  if (!Icons?.[icon]?.source) {
    return
  }

  const iconSource = Icons[icon].source.replace(/currentColor/g, color)
  const encoded = `background-image: url('data:image/svg+xml,${iconSource.replace(
    /<|>|#/g,
    (match) => {
      return {
        '<': '%3C',
        '>': '%3E',
        '#': '%23',
      }[match] as string
    }
  )}');`
  return encoded
}

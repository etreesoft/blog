import { css } from '@emotion/react'

import {
  AlignType,
  DescriptionTypes,
  RowHeightType,
  StyleProps,
} from '@/components/Description'

import { Colors } from './colors'
import { Fonts, StyleFontWeight } from './fonts'

export interface CellStyleProps {
  type?: DescriptionTypes
  cellType?: 'form'
  parent?: 'th' | 'td'
  hasInner?: boolean
  size?: 'small' | 'medium' | 'large'
  align?: AlignType
  border?: boolean
}

export const common = {
  cellPaddingVertical: 15,
  cellPaddingHorizontal: 3, // vw 기준
  cellPaddingHorizontalMax: 82, // px 기준
  cellHeight: 60,
  inner: {
    cellHeight: 50,
  },
}

export const commonStyle = ({ align, border, rowType }: StyleProps) => css`
  width: 100%;
  table-layout: fixed;
  border-inline-style: hidden;
  border-top: 2px solid ${Colors.BLACK};

  th,
  td {
    box-sizing: border-box;
    vertical-align: top;

    /* 내부 data-cell-text div의 height: 100% 처리의 기준용 최소 높이. */
    height: 10px;

    ${border &&
    css`
      border-inline: 1px solid;
      border-inline-color: ${Colors.COOL_GRAY_10};
    `}

    > [data-cell-text] {
      ${Fonts.REGULAR_16};
      padding-left: 20px;
      padding-right: 20px;
    }
  }

  th {
    > [data-cell-text] {
      font-weight: ${StyleFontWeight.BOLD};
      padding: ${common.cellPaddingVertical}px 40px;
    }
  }

  td {
    text-align: ${align};
    > [data-cell-text] {
      padding: ${common.cellPaddingVertical}px
        min(
          ${common.cellPaddingHorizontal}vw,
          ${common.cellPaddingHorizontalMax}px
        );
    }
  }

  thead[data-size='large'] th {
    > [data-cell-text] {
      ${Fonts.BOLD_20};
    }
  }

  thead > tr,
  tbody > tr {
    ${rowStyle({ rowType })}
  }
`

export const cellStyle = ({
  type,
  cellType,
  align,
  hasInner,
}: CellStyleProps) => css`
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: ${common.cellHeight}px;
  height: 100%;
  word-break: break-all;
  ${align && `text-align: ${align};`}

  ${hasInner &&
  css`
    &&& {
      padding: 0;
    }
  `}

  ${type === 'search' &&
  css`
    min-height: 0;
    padding: 0;
    padding-right: 20px;
  `}

  ${cellType === 'form' &&
  css`
    &&& {
      padding-right: 10px;
      & > div > :first-of-type:last-of-type {
        margin-right: 0;
      }
    }
  `}
`

export const rowStyle = ({
  emphasisBorderBottom,
  rowType,
}: {
  emphasisBorderBottom?: boolean
  rowType?: RowHeightType
}) => css`
  display: ${rowType} !important;
  ${emphasisBorderBottom &&
  css`
    > th,
    > td {
      border-bottom-color: ${Colors.COOL_GRAY_20};
    }
  `}

  ${rowType === 'dense' &&
  css`
    > th,
    > td {
      > [data-cell-text] {
        min-height: 50px;
        padding-top: 12px;
        padding-bottom: 9px;

        [data-form] {
          margin-top: -3px;
        }
      }
    }
    > th > [data-cell-text] {
    }
  `}
`

export const defaultTableStyle = (props: StyleProps) => css`
  ${commonStyle(props)};

  th,
  td {
    border-bottom: 1px solid ${Colors.COOL_GRAY_10};
  }

  th {
    background-color: ${Colors.COOL_GRAY_10_50};
  }
`

export const innerTableStyle = ({
  align = 'center',
  ...rest
}: StyleProps) => css`
  ${commonStyle({ ...rest })};
  border-top: 0;

  && {
    th,
    td {
      vertical-align: top;
      text-align: ${align};
    }

    th {
      background-color: ${Colors.COOL_GRAY_10_20};
      > [data-cell-text] {
        ${align !== 'center' &&
        css`
          padding-left: 20px;
          padding-right: 20px;
        `}
      }
    }

    td {
      background-color: #fff;
    }

    thead {
      th,
      td {
        background-color: ${Colors.COOL_GRAY_10_20};
      }
    }
  }
`

export const transparentTableStyle = ({ border, ...rest }: StyleProps) => css`
  ${commonStyle(rest)};

  thead {
    th {
      border-bottom: 2px solid ${Colors.BLACK};
    }
  }

  th,
  td {
    background-color: #fff;
  }
`

export const searchTableStyle = (props: StyleProps) => css`
  width: 100%;

  th,
  td {
    box-sizing: border-box;
    vertical-align: top;
    height: 54px;
    padding: 8px 0;

    > [data-cell-text] {
      ${cellStyle(props)};
      padding-right: 0;
    }
  }

  th {
    text-align: left;
    white-space: nowrap;

    /* Text 너비만큼만 너비 지정되기 위한 trick */
    width: 1px;
    > [data-cell-text] {
      padding-right: 40px;
      font-weight: ${StyleFontWeight.BOLD};
    }
  }

  td {
    &:last-child > [data-cell-text] {
      padding-right: 0;
    }
  }
`

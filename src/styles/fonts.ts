import { css } from '@emotion/react'

import { CDN_BASE_URL } from '../constants'

export enum StyleFontWeight {
  LIGHT = 300,
  REGULAR = 400,
  BOLD = 600,
}

export const SIGNATURE_FONTS = css`
  @font-face {
    font-family: 'Kia Signature';
    src: url('${CDN_BASE_URL}/fonts/KiaSignatureLight.woff') format('woff'),
      url('${CDN_BASE_URL}/fonts/KiaSignatureLight.woff2') format('woff2');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Kia Signature';
    src: url('${CDN_BASE_URL}/fonts/KiaSignatureRegular.woff') format('woff'),
      url('${CDN_BASE_URL}/fonts/KiaSignatureRegular.woff2') format('woff2');
    font-weight: 400;
    font-style: normal;
  }

  @font-face {
    font-family: 'Kia Signature';
    src: url('${CDN_BASE_URL}/fonts/KiaSignatureBold.woff') format('woff'),
      url('${CDN_BASE_URL}/fonts/KiaSignatureBold.woff2') format('woff2');
    font-weight: 600;
    font-style: normal;
  }
`

export const FONT_FAMILY_SANS_SERIF =
  '-apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue" ,Arial, sans-serif'

export const FONT_FAMILY_SIGNATURE = `'Kia Signature', ${FONT_FAMILY_SANS_SERIF}`

export const makeFonts = (
  fontSize: string,
  fontWeight: number | string,
  lineHeight: number | string
) => {
  return css`
    font-size: ${fontSize};
    font-weight: ${fontWeight};
    line-height: ${lineHeight};
  `
}

export const Fonts = {
  BOLD_24: makeFonts('24px', StyleFontWeight.BOLD, '1.4'),
  BOLD_22: makeFonts('22px', StyleFontWeight.BOLD, '1.4'),
  BOLD_20: makeFonts('20px', StyleFontWeight.BOLD, '1.4'),
  BOLD_18: makeFonts('18px', StyleFontWeight.BOLD, '1.4'),
  BOLD_16: makeFonts('16px', StyleFontWeight.BOLD, '1.4'),
  BOLD_14: makeFonts('14px', StyleFontWeight.BOLD, '1.4'),
  BOLD_13: makeFonts('13px', StyleFontWeight.BOLD, '1.4'),
  BOLD_12: makeFonts('12px', StyleFontWeight.BOLD, '1.4'),

  REGULAR_20: makeFonts('20px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_18: makeFonts('18px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_16: makeFonts('16px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_14: makeFonts('14px', StyleFontWeight.LIGHT, '1.4'),
  REGULAR_13: makeFonts('13px', StyleFontWeight.LIGHT, '1.4'),
  REGULAR_12: makeFonts('12px', StyleFontWeight.LIGHT, '1.4'),
} as const

export type FontsKey = keyof typeof Fonts

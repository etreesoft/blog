import { css } from '@emotion/react'

import { Colors } from './colors'
import { Fonts } from './fonts'

export const FORM_CONTROL_STYLE = {
  height: 38,
  width: 200,
  bgColor: {
    disabled: Colors.BLACK_2,
    readonly: Colors.BLACK_2,
  },
  borderColor: {
    primary: Colors.BLACK,
    error: Colors.RED,
    disabled: Colors.COOL_GRAY_20,
    readonly: Colors.BLACK_20,
  },
  color: {
    primary: Colors.BLACK,
    error: Colors.BLACK,
    disabled: Colors.COOL_GRAY_20,
    readonly: Colors.BLACK,
    placeholder: Colors.COOL_GRAY_20,
  },
} as const

const { height, borderColor, bgColor, color } = FORM_CONTROL_STYLE

export const FORM_CONTROL = css`
  ${Fonts.REGULAR_14};
  box-sizing: border-box;
  position: relative;
  width: 100%;
  min-height: ${height}px;
  padding: 5px 14px 4px;
  border-radius: 4px;
  border: 1px solid ${borderColor.primary};
  line-height: normal;
  color: ${color.primary};
  outline: 0;

  &::placeholder {
    color: ${color.placeholder};
  }

  &:read-only {
    background-color: ${bgColor.readonly};
    border-color: ${borderColor.readonly};
    color: ${color.readonly};
  }

  &:disabled {
    border-color: ${borderColor.disabled};
    background-color: ${bgColor.disabled};
    color: ${color.disabled};
  }
`

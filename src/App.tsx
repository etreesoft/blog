import { BrowserRouter } from 'react-router-dom'
import routes from '@/router'
import RouteIndex from '@/router/RouteIndex';
import Layout from '@/layout/Layout'

import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Layout>
          <RouteIndex routes={routes.sort()} />
        </Layout>
      </BrowserRouter>
    </div>
  );
}

export default App;

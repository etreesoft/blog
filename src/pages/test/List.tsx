import DefaultLayout from '@/components/layout/DefaultLayout'
import routes from "@/router";
import {Link} from "react-router-dom";

export default function SampleList() {
	return (
		<DefaultLayout title='테스트 리스트'>
			{routes
					.filter((route) => route.path.startsWith('/test') && route.path !== '/test')
					.map((route, i) => (
							(route.type === 'list' ||
									route.type === undefined) ?
									<div key={i}>
										<Link to={route.path}>
											{route.page}({route.path})
										</Link>
									</div> :
									<template key={i}></template>
					))
			}
		</DefaultLayout>
	)
}

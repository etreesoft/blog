import DefaultLayout from '@/components/layout/DefaultLayout'
import Typography from '@/components/Typography'

export default function SampleDetail() {
	const { Text } = Typography
	return (
		<DefaultLayout title='테스트 상세1'>
			<Text></Text>
		</DefaultLayout>
	)
}

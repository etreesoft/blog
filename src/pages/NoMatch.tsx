import { css } from '@emotion/react'
import { useLocation } from 'react-router-dom'

export default function NoMatch() {
  const location = useLocation()

  return (
    <div>
      <div
        css={css`
          padding: 20px;
          background-color: #eee;

          h1 {
            margin-bottom: 10px;
            font-size: 20px;
            font-weight: 700;
          }
        `}
      >
        <h1>{location.pathname}</h1>
        <p>페이지를 찾을 수 없습니다.</p>
      </div>
    </div>
  )
}

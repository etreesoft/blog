import DefaultLayout from '@/components/layout/DefaultLayout'
import routes from "@/router";
import {Link} from "react-router-dom";

export default function SampleList() {
	return (
		<DefaultLayout title='샘플 리스트'>
			{routes
					.filter((route) => route.path.startsWith('/sample') && route.path !== '/sample')
					.map((route, i) => (
							(route.type === 'list' ||
									route.type === undefined) ?
									<div key={i}>
										<Link to={route.path}>
											{route.page}({route.path})
										</Link>
									</div> :
									<template key={i}></template>
					))
			}
		</DefaultLayout>
	)
}

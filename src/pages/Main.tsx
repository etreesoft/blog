import routes from '@/router'
import { Link } from 'react-router-dom'

export default function Main() {
  return (
    <div>
      <h1>테스트 메인</h1>
      {routes
        .map((route, i) => (
          (route.type === 'list' ||
            route.type === undefined) ?
          <div key={i}>
            <Link to={route.path}>
              {route.page}({route.path})
            </Link>
          </div> :
              <template key={i}></template>
        ))
      }
    </div>
  )
}

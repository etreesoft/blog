import { addDays, addMonths } from 'date-fns'

import { CSSProperties, HTMLAttributes } from 'react'

export interface HTMLStyleProps {
  style?: CSSProperties
  className?: string
}

export interface HTMLElementProps<T> extends HTMLStyleProps {
  htmlAttr?: HTMLAttributes<T>
}
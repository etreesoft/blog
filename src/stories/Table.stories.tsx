import { ComponentMeta, ComponentStory } from '@storybook/react'

import Pagination from '@/components/Pagination'
import Table, { Columns } from '@/components/Table'
import Button from '@/components/atomic/Button'
import { dateFormat } from '@/utils/date'

export default {
  title: 'Data Display/Table',
  args: {},
  argTypes: {
    size: {
      options: ['medium', 'large'],
      control: { type: 'radio' },
    },
  },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
} as ComponentMeta<typeof Table>

interface Response {
  id: number
  date: string
  status: string
  change: Array<{ status: string; value: string }>
  detailId: number
}

const listRowspan: Response[] = [
  {
    id: 0,
    date: '2022-10-31T23:59:59',
    status: '보험서류',
    change: [
      { status: 'before', value: '미검수' },
      { status: 'after', value: '승인' },
    ],
    detailId: 0,
  },
  {
    id: 1,
    date: '2022-09-12T15:59:59',
    status: '매도제시 상태',
    change: [
      { status: 'before', value: '미검수' },
      { status: 'after', value: '반려 (반려사유 : 2022년도 서류제출필요)' },
    ],
    detailId: 1,
  },
]

const list = listRowspan.map((item) => ({ ...item, worker: 'kia001' }))

export const Default: ComponentStory<typeof Table> = () => {
  const defaultColumns: Columns<
    Response & {
      worker: string
    }
  > = [
    {
      title: '변경 발생일',
      render: (_, { date }) => dateFormat(date),
      width: 150,
      fixed: 'left', // 왼쪽 고정
    },
    {
      dataIndex: 'status',
      title: '변경 정보',
    },
    {
      title: '변경 사항',
      cellAlign: 'left',
      width: 400,
      render: (_, { change }) => {
        return (
          <ul>
            {change?.map(({ status, value }, i) => (
              <li key={i}>
                <b>{status === 'before' ? '변경 전' : '변경 후'} - </b>
                {value}
              </li>
            ))}
          </ul>
        )
      },
    },
    {
      dataIndex: 'worker',
      title: '작업자 ID',
      width: 150,
      fixed: 'right', // 오른쪽 고정
    },
  ]

  return (
    <>
      <h1>Table</h1>
      <div className='explain'>
        <p>
          <i>Table</i>컴포넌트는 Data 모델 Type에 의존하므로 반드시 타입을
          연결해주세요. <br /> ex) <i>&lt;Table&lt;Model&gt; ... /&gt;</i>
          <br />
          <br />
          <b>Antd Table을 기반으로 개발되었습니다. 기본 사용법은</b>{' '}
          <a
            href='https://ant.design/components/table'
            target='_blank'
            rel='noreferrer'
          >
            공식 API 문서
          </a>{' '}
          를 참고해주세요.
        </p>

        <ul>
          <li>
            <b>columns</b> - <i>Table</i>에 주입한 Data 모델을 상속받습니다.
            <ul>
              <li>
                <b>id:</b> string | number - 반드시 고유해야하는 Key
              </li>
              <li>
                <b>dataKey?:</b> keyof Model - Data 모델의 Key(출력하고자 하는
                property name) <br />ㄴ <i>render</i>를 사용하지 않는 경우 필수
              </li>
              <li>
                <b>title?:</b> React.ReactNode - Head Cell 내용(Column 제목)
              </li>
              <li>
                <b>render?</b>: (value: any, record: Model) =&gt;
                React.ReactNode
              </li>
              <li>
                <b>align?</b>: 'left' | 'center' | 'right' - Head 셀 정렬
              </li>
              <li>
                <b>cellAlign?</b>: 'left' | 'center' | 'right' - Body 셀 정렬
              </li>
            </ul>
          </li>
          <li>
            <b>dataSource&lt;Model[]&gt;: </b> <i>Table</i>에 주입한 Data 모델을
            상속받습니다. 모델과 동일한 형태의 배열을 주입해야합니다.
          </li>
        </ul>
      </div>

      <h1>기본 ( + Pagination)</h1>
      <Table<Response & { worker: string }>
        dataSource={list}
        columns={defaultColumns}
      />
      <Pagination />

      <hr />

      <h1>Checked Column + RowSpan + 행 높이 조절</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>rowSelection?:</b> <i>Object</i>{' '}
            <a
              href='https://ant.design/components/table#rowselection'
              target='_blank'
              rel='noreferrer'
            >
              Docs
            </a>
          </li>
          <li>
            <b>rowType?:</b>
            <i>'default(default)' | 'dense' | 'headerDense' | 'bodyDense'</i>
            <ul>
              <li>
                <s>dense:</s> header, body 모든 행 높이 좁게 표현
              </li>
              <li>
                <s>headerDense:</s> header 행 높이 좁게 표현
              </li>
              <li>
                <s>bodyDense:</s> body 행 높이 좁게 표현
              </li>
            </ul>
          </li>
          <li>
            <b>borderType?:</b>
            <i>'default(default)' | 'header' | 'body' | 'all'</i>
          </li>
        </ul>
      </div>
      <Table<Response>
        rowType='headerDense'
        borderType='header'
        dataSource={list}
        rowSelection={{
          onChange: (selectedKeys, selected) => {
            console.log(
              `selectedKeys: ${selectedKeys} | `,
              'selected: ',
              selected
            )
          },
          getCheckboxProps: (record) => ({
            disabled: record.id === 0, // Disabled 조건 예시
          }),
        }}
        columns={[
          {
            title: '변경 발생일',
            rowSpan: 2,
            render: (_, { date }) => dateFormat(date),
          },
          {
            title: '비고',
            children: [
              // colSpan cell data - children 으로 선언
              {
                dataIndex: 'status',
                title: '변경 정보',
              },
              {
                title: '변경 사항',
                cellAlign: 'left',
                render: (_, { change }) => {
                  return (
                    <ul>
                      {change?.map(({ status, value }, i) => (
                        <li key={i}>
                          <b>
                            {status === 'before' ? '변경 전' : '변경 후'} -{' '}
                          </b>
                          {value}
                        </li>
                      ))}
                    </ul>
                  )
                },
              },
            ],
          },
          {
            title: '상세',
            rowSpan: 2,
            render: (_, { detailId }) => {
              return <Button size={26}>조회</Button>
            },
          },
        ]}
      />

      <hr />

      <h1>틀고정 (Col)</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>scroll?:</b> X축 스크롤 기준 너비 설정 <br />
            <i>{`{
              x|y?: string | number | undefined;
              y?: string | number | undefined;
            }`}</i>
          </li>
          <li>
            <b>columns - col:</b>
            <ul>
              <li>
                <s>width?:</s> number
                <p>
                  모든 컬럼의 <i>width</i>를 선언할 필요는 없습니다. 고정 너비를
                  가져야 하는 컬럼만 <i>width</i>를 선언해주세요. <br />
                  (적어도 한개 이상의 컬럼은 width 가 선언되지 않아야 열 고정이
                  정상 동작합니다.)
                </p>
              </li>
              <li>
                <s>fixed?:</s> <i>'left' | 'right'</i>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <Table<Response & { worker: string }>
        dataSource={list}
        scroll={{ x: 1000 }} // x축 스크롤 기준 너비 설정
        columns={defaultColumns}
      />

      <hr />

      <h1>Row - 특정 행 Style 지정</h1>
      <div className='explain'>
        <p>
          <i>rowClassName, onRow</i> 속성을 사용하여 적용합니다. 미리 제작되어
          제공되는 rowStyle은 아래와 같습니다. 이외의 추가적인 case 가 필요한
          경우 요청해주세요.
        </p>
        <ul>
          <li>
            <b>rowClassName?:</b> (onRow에서 사용) 'danger'
          </li>
        </ul>
      </div>
      <Table<Response & { worker: string }>
        dataSource={list}
        columns={defaultColumns}
        onRow={(record) => {
          return {
            className: record.id === 0 ? 'danger' : '',
          }
        }}
      />
    </>
  )
}

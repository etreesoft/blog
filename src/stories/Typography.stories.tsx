import { ComponentStory } from '@storybook/react'

import Typography from '@/components/Typography'
import Button from '@/components/atomic/Button'
import Select from '@/components/atomic/Select'
import { numberFormat } from '@/utils/common'

export default {
  title: 'Foundation/Typography',
  args: {},
}

type levelType = 1 | 2 | 3 | 4 | 5 | 6 | undefined

export const Title: ComponentStory<typeof Typography.Title> = (args) => {
  const { Title } = Typography

  return (
    <>
      <h1>Title(Heading)</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>gap?:</b> 14 | 20(default) - 하단 여백
          </li>
          <li>
            <b>extra?:</b> React.ReactNode
          </li>
          <li>
            <b>extraRight?:</b> React.ReactNode
          </li>
          <li>
            <b>level?:</b> 1 | 2 | 3 | 4 | 5 | 6 (default: 1)
          </li>
          <li>
            <b>subText?:</b> string
          </li>
          <li>
            <b>subTextPlacement?:</b> 'right'(default) | 'bottom'
          </li>
        </ul>
      </div>

      {[1, 2, 3].map((level) => (
        <Title key={level} level={level as levelType}>
          헤는 가득 어머니, 둘 릴케 했던 별 계십니다. (level: {level})
        </Title>
      ))}

      <hr />

      <h1>Title + subText</h1>
      <Title subText='이미지 클릭시 확대하여 볼 수 있습니다.'>이미지</Title>

      <hr />

      <h1>Title + subText(subTextPlacement: bottom)</h1>
      <Title
        subText='이미지 클릭시 확대하여 볼 수 있습니다.'
        subTextPlacement='bottom'
      >
        이미지
      </Title>

      <hr />

      <h1>Title + extra + extraRight</h1>

      <Title
        extra={<Button primary>전시화면 미리보기</Button>}
        extraRight={<Button icon='excel'>Export</Button>}
      >
        차량 정보
      </Title>
    </>
  )
}
export const Text: ComponentStory<typeof Typography.Text> = (args) => {
  const { Text } = Typography

  return (
    <>
      <h1>Text</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>gap?:</b> 14 | 20(default) - 하단 여백
          </li>
          <li>
            <b>extra?:</b> React.ReactNode
          </li>
          <li>
            <b>extraRight?:</b> React.ReactNode
          </li>
          <li>
            <b>type?:</b> <i>keyof typeof Fonts</i> (default: 'REGULAR_16')
          </li>
          <li>
            <b>color?:</b> <i>keyof typeof Colors</i> (default: 'BLACK')
          </li>
        </ul>
      </div>

      <Text
        type='BOLD_22'
        gap={14}
        extra={
          <>
            <Button primary size={40}>
              2건 매각하기
            </Button>
            <Button size={40}>취소</Button>
          </>
        }
        extraRight={
          <>
            <Button icon='excel'>Export</Button>
            <Select
              options={[
                { value: 10, label: '10개씩' },
                { value: 30, label: '30개씩' },
                { value: 50, label: '50개씩' },
              ]}
              value={10}
              size='small'
              width={80}
            />
          </>
        }
      >
        총 {numberFormat(50)}건
      </Text>

      <hr />

      <Text
        gap={20}
        extra={
          <>
            <Select
              options={[{ value: 'none', label: '오전 09:00' }]}
              value='none'
              width={140}
              size='small'
            />
            {'~'}
            <Select
              options={[{ value: 'none', label: '오전 10:00' }]}
              value='none'
              width={140}
              size='small'
            />
          </>
        }
      >
        배정가능일시
      </Text>

      <hr />

      <Text type='REGULAR_18' gap={20}>
        헤는 가득 어머니, 둘 릴케 했던 별 계십니다.
      </Text>

      <hr />

      <Text type='REGULAR_14' align='center'>
        보험금 및 수리(견적)비 출처에 따라서 ‘가입한 보험사에서 지급된 경우(내차
        보험)’와 ‘다른 차량 보험에서 지급된 경우(상대 보험)’로 나뉘어
        제공됩니다.
        <br />
        자동차사고로 상대 차량 또는 재물에 발생한 손해를 내 보험금에서 지급된
        경우의 정보를 제공합니다.
      </Text>

      <hr />

      <Text type='REGULAR_14' align='center' color='COOL_GRAY_50'>
        *쌍방과실로 해당 자동차의 손상, 수리 기록이 내차 보험과 상대 보험에서
        동시에 처리된 경우에는 ‘내차 보험’에만 표시되고 ‘상대 보험’에서는
        생략됩니다.
      </Text>
    </>
  )
}

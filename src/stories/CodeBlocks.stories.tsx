import { ComponentMeta, ComponentStory } from '@storybook/react'

import CodeBlocks from '@/components/atomic/CodeBlocks'

const jsxCode = `class HelloMessage extends React.Component {
  handlePress = () => {
    alert('Hello')
  }
  render() {
    return (
      <div>
        <p>Hello {this.props.name}</p>
        <button onClick={this.handlePress}>Say Hello</button>
      </div>
    );
  }
}

ReactDOM.render(
  <HelloMessage name="Taylor" />, 
  mountNode 
);
`

export default {
  title: 'Atomic/CodeBlocks',
  component: CodeBlocks,
  args: {
    language: 'tsx',
    showLineNumbers: true,
    theme: 'dracula',
    wrapLines: false,
    text: `npm run dev`,
    highlight: '2-4',
  },
} as ComponentMeta<typeof CodeBlocks>

export const Default: ComponentStory<typeof CodeBlocks> = (args) => {
  return (
    <>
      <h1>CodeBlocks</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>codeStyle?:</b> block(default) | copy | code
          </li>
          <li>
            <b>language?:</b> keyof typeof Languages (ex: 'bash')
          </li>
          <li>
            <b>text:</b> string(ex: 'npm run dev')
          </li>
          <li>
            <b>showLineNumbers?:</b> boolean(default: false)
          </li>
          <li>
            <b>theme?:</b> dracula(default)
          </li>
          <li>
            <b>wrapLines?:</b> boolean(default: false)
          </li>
          <li>
            <b>highlight?:</b> string(ex: '3', '2-4', '2-4,7,8,9,10')
          </li>
        </ul>
      </div>

      <h1>CodeBlock</h1>
      <CodeBlocks
          language={args.language}
          showLineNumbers={args.showLineNumbers}
          text={jsxCode}
          theme={args.theme}
          wrapLines={args.wrapLines}
          highlight={args.highlight}
      />

      <h1>CopyBlock</h1>
      <CodeBlocks
          codeStyle='copy'
          language={args.language}
          showLineNumbers={args.showLineNumbers}
          text={jsxCode}
          theme={args.theme}
          wrapLines={args.wrapLines}
          highlight={args.highlight}
      />

      <h1>Code</h1>
      <CodeBlocks
          codeStyle='code'
          text={args.text}
      />
      
    </>
  )
}
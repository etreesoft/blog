import { css } from '@emotion/react'

import { ColorsKey, Colors as colors } from '@/styles/colors'

export default {
  title: 'Foundation/Colors',
}

export const Colors = () => {
  const colorsKey = Object.keys(colors)

  return (
    <>
      <h1>Colors</h1>
      <div css={styles.container}>
        {colorsKey.map((color) => {
          return (
            <span css={styles.chip({ color: color as ColorsKey })} key={color}>
              <i>{color}</i>
            </span>
          )
        })}
      </div>
    </>
  )
}

const styles = {
  container: css`
    display: flex;
    flex-wrap: wrap;
    max-width: 800px;
    gap: 20px;
  `,
  chip: ({ color }: { color: ColorsKey }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100px;
    height: 50px;
    padding: 10px;
    background-color: ${colors[color]};
    word-break: break-all;
    text-align: center;
    i {
      color: ${colors[color]};
      filter: invert(100%);
    }
  `,
}

import { css } from '@emotion/react'
import { ComponentMeta, ComponentStory } from '@storybook/react'

import Grid from '@/components/layout/Grid'

const spacingOption = {
  options: ['small', 'medium', 'large', 'none'],
  control: { type: 'radio' },
}

export default {
  title: 'Layout/Grid',
  args: {
    spacing: 'none',
    gap: 'small',
  },
  argTypes: {
    spacing: spacingOption,
    gap: spacingOption,
  },
} as ComponentMeta<typeof Grid>

export const Default: ComponentStory<typeof Grid> = (args) => {
  return (
    <>
      <h1>Grid</h1>
      <div className='explain'>
        <p>
          영역간 상/하 여백은 HTML 선형순서에 따른 <b>CSS 쌓임 맥락</b>으로
          자동으로 적용되는것이 기본 설정입니다.
          <br />위 조건으로 충족되지 않는 요소간 상/하 여백이 필요하거나,
          <br /> 여러개의 Column 으로 구분된 레이아웃을 생성해야 할 경우
          사용해주세요.
        </p>
        <ul>
          <li>
            mt-top-10 , pt-top-10 등의 여백용 ClassName 선언은 금지됩니다.
          </li>
          <li>인라인 스타일로 margin, padding을 선언하는것은 지양합니다.</li>
        </ul>
        <hr />
        <p>
          <b>(interface)Spacing:</b>{' '}
          <i>'small' | 'medium' | 'large' | 'none' | number</i>
        </p>
        <p>
          <b>(interface)SpacingType:</b> <i>Spacing | [Spacing, Spacing]</i>
        </p>

        <ul>
          <li>
            <b>spacing:</b> <i>SpacingType</i>
          </li>
          <li>
            <b>gap?:</b> <i>SpacingType</i>
          </li>
        </ul>
        <p>
          <b>gap</b> 속성은 container에서 상속받는것이 기본이지만, 필요에 따라
          Row에 개별 설정이 가능합니다.
        </p>
      </div>

      <hr />

      <div css={styles.container}>
        <h1>기본(균등 / auto + grow 혼합, Row 개별 gap 설정)</h1>
        <Grid {...args}>
          <Grid.Row gap={['medium', 10]}>
            <Grid.Col>1</Grid.Col>
            <Grid.Col>2</Grid.Col>
          </Grid.Row>
          <Grid.Row>
            <Grid.Col flex='auto'>auto</Grid.Col>
            <Grid.Col flex='grow'>Grow</Grid.Col>
          </Grid.Row>
        </Grid>

        <hr />

        <h1>3단 / 2단 혼합 (균등)</h1>
        <Grid {...args}>
          <Grid.Row>
            <Grid.Col>1</Grid.Col>
            <Grid.Col>2</Grid.Col>
            <Grid.Col>3</Grid.Col>
          </Grid.Row>
          <Grid.Row>
            <Grid.Col>4</Grid.Col>
            <Grid.Col>5</Grid.Col>
          </Grid.Row>
        </Grid>

        <hr />

        <h1>2단 좌/우 정렬</h1>
        <Grid align='justify'>
          <Grid.Row>
            <Grid.Col>
              <div>1</div>
            </Grid.Col>
            <Grid.Col>
              <div>2</div>
            </Grid.Col>
          </Grid.Row>
        </Grid>

        <hr />

        <h1>1단 우측 정렬</h1>
        <Grid align='right'>
          <Grid.Row>
            <Grid.Col>
              <div>1</div>
            </Grid.Col>
          </Grid.Row>
        </Grid>

        <hr />

        <h1>1단 좌측 정렬</h1>
        <Grid align='left'>
          <Grid.Row>
            <Grid.Col>
              <div>1</div>
            </Grid.Col>
          </Grid.Row>
        </Grid>

        <hr />

        <h1>가운데 정렬</h1>
        <Grid align='center'>
          <Grid.Row>
            <Grid.Col>
              <div>1</div>
            </Grid.Col>
          </Grid.Row>
        </Grid>
      </div>
    </>
  )
}

const styles = {
  container: css`
    [class$='Col'] {
      padding: 40px;
      text-align: center;
      background-color: #eee;
    }
  `,
}

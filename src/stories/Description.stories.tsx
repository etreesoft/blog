import { ComponentMeta, ComponentStory } from '@storybook/react'

import Description from '@/components/Description'
import Radio from '@/components/atomic/Radio'

export default {
  title: 'Data Display/Description',
  component: Description,
  args: {
    bottomBorder: false,
    align: 'left',
    border: false,
  },
  parameters: {
    controls: {
      exclude: ['type', 'colWidth', 'inner'],
    },
  },
} as ComponentMeta<typeof Description>

export const Default: ComponentStory<typeof Description> = (args) => {
  const { Thead, Tbody, Row, Th, Td } = Description

  return (
    <>
      <h1>Description</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>border?:</b>
            <s>boolean(default: false)</s> - 셀 border 처리 여부
          </li>
        </ul>
      </div>
      <Description align='left' colWidth={[200, 'auto', 200, 'auto']} {...args}>
        <Tbody>
          <Row>
            <Th>전시상품 ID</Th>
            <Td>displayID</Td>
            <Th>등록일</Th>
            <Td>
              {[
                { label: '전체', value: 'all' },
                { label: '최근 7일', value: 'latest-7day' },
                { label: '기간 선택', value: 'date-input' },
              ].map(({ label, value }, i) => (
                <Radio
                  key={i}
                  label={label}
                  value={value}
                  defaultChecked={value === 'date-input'}
                  name='register-date'
                />
              ))}
            </Td>
          </Row>
          <Row>
            <Th>차량이름</Th>
            <Td>디 올 뉴 니로 2022</Td>
            <Th>차량번호</Th>
            <Td>12 라 1212</Td>
          </Row>
          <Row>
            <Th>출고일</Th>
            <Td>2022-12-13</Td>
            <Th>차량트립</Th>
            <Td>1.6 하이브리드 시그니처</Td>
          </Row>
          <Row>
            <Th>카테고리</Th>
            <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
            <Th>연식</Th>
            <Td>2023년식</Td>
          </Row>
        </Tbody>
      </Description>

      <h1>Default (+ Inner)</h1>
      <Description colWidth={[250, 'auto', 200, 'auto']} {...args}>
        <Thead>
          <Row emphasisBorderBottom>
            <Th>진행중인 주문번호</Th>
            <Td>304902903490234</Td>
            <Th>주문상태</Th>
            <Td>결제완료</Td>
          </Row>
        </Thead>
        <Tbody>
          <Row>
            <Th>과거주문이력</Th>
            <Td hasInner colSpan={3}>
              <Description type='inner' border>
                <Thead>
                  <Row>
                    <Th>주문번호</Th>
                    <Th>날짜</Th>
                    <Th>요약</Th>
                  </Row>
                </Thead>
                <Tbody>
                  <Row>
                    <Td>123456789</Td>
                    <Td>2022-12-01 12:59:59</Td>
                    <Td>환불완료</Td>
                  </Row>
                </Tbody>
              </Description>
            </Td>
          </Row>
        </Tbody>
      </Description>

      <h1>Default (+ Dense Row)</h1>
      <div className='explain'>
        <p>
          <i>rowHeight="dense"</i>를 추가하면 행의 높이가 줄어듭니다. rowSpan
          으로 행높이를 줄여서 출력해야 하는 경우 사용해주세요.
        </p>
      </div>
      <Description colWidth={[200]}>
        <Tbody>
          <Row>
            <Th>세부사항 및 금액</Th>
            <Td hasInner>
              <Description {...args} type='inner' colWidth={[160]}>
                <Thead>
                  <Row rowType='dense'>
                    <Th rowSpan={2}>차량 가격</Th>
                    <Th rowSpan={2}>탁송료</Th>
                    <Th rowSpan={2}>매도수수료</Th>
                    <Th colSpan={4}>이전등록비</Th>
                  </Row>
                  <Row rowType='dense'>
                    <Th>취등록세</Th>
                    <Th>공채매입비</Th>
                    <Th>명의이전대행비</Th>
                    <Th>인증지대</Th>
                  </Row>
                </Thead>
                <Tbody>
                  <Row>
                    <Td>16,500,000원</Td>
                    <Td>200,000원</Td>
                    <Td>165,000원</Td>
                    <Td>16,500,000원</Td>
                    <Td>200,000원</Td>
                    <Td>165,000원</Td>
                    <Td>165,000원</Td>
                  </Row>
                </Tbody>
              </Description>
            </Td>
          </Row>
        </Tbody>
      </Description>

      <h1>Transparent</h1>
      <Description
        colWidth={[200, 'auto', 200, 'auto']}
        {...args}
        type='transparent'
      >
        <Tbody>
          <Row>
            <Th>전시상품 ID</Th>
            <Td>displayID</Td>
            <Th>등록일</Th>
            <Td>2022-12-01 12:59:59</Td>
          </Row>
          <Row>
            <Th>차량이름</Th>
            <Td>디 올 뉴 니로 2022</Td>
            <Th>차량번호</Th>
            <Td>12 라 1212</Td>
          </Row>
          <Row>
            <Th>출고일</Th>
            <Td>2022-12-13</Td>
            <Th>차량트립</Th>
            <Td>1.6 하이브리드 시그니처</Td>
          </Row>
        </Tbody>
      </Description>

      <h1>Transparent - Thead(size='large') </h1>
      <div className='explain'>
        <ul>
          <li>
            Thead
            <ul>
              <li>
                <b>size:</b> 'small' | 'medium'(default) | 'large'
              </li>
            </ul>
          </li>
        </ul>
      </div>

      <Description {...args} type='transparent'>
        <Thead size='large'>
          <Row>
            <Th>차량 ID</Th>
            <Th>매입유형</Th>
            <Th>매입신청일</Th>
            <Th>평가유형</Th>
          </Row>
        </Thead>
        <Tbody>
          <Row>
            <Td>CPO494923423488</Td>
            <Td>개인</Td>
            <Td>2022.12.02 - 14:07:56</Td>
            <Td>데이터</Td>
          </Row>
        </Tbody>
      </Description>
    </>
  )
}

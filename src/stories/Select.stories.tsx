import { ComponentMeta, ComponentStory } from '@storybook/react'

import { useState } from 'react'

import Select, { Option } from '@/components/atomic/Select'

export default {
  title: 'Atomic/Select',
  component: Select,
  args: {
    size: 'medium',
  },
  argTypes: {
    size: {
      options: ['small', 'medium', 'large'],
      control: { type: 'radio' },
    },
  },
} as ComponentMeta<typeof Select>

const options: Option[] = [
  { value: 1, label: '가나다' },
  { value: 2, label: '홍길동' },
]

export const Default: ComponentStory<typeof Select> = (args) => {
  const [value, setValue] = useState<number>(1)
  const [errorValue, setErrorValue] = useState<number>(1)

  return (
    <>
      <h1>Select</h1>

      <Select
        options={options}
        value={value}
        onChange={(selection) => {
          setValue(selection.value as number)
          console.log(selection)
        }}
        {...args}
      />

      <Select
        disabled
        options={[{ value: 'none', label: '선택 불가(disabled)' }]}
        value='none'
        width={180}
        {...args}
      />

      <hr />

      <h1>오류</h1>
      <Select
        options={options}
        value={errorValue}
        isValid={errorValue === 2}
        errorMessage='홍길동을 선택해 주세요.'
        onChange={(selection) => {
          setErrorValue(selection.value as number)
          console.log(selection)
        }}
      />

      <hr />

      <h1>Size</h1>
      <Select
        options={[
          { value: 10, label: '10개씩' },
          { value: 30, label: '30개씩' },
          { value: 50, label: '50개씩' },
        ]}
        value={10}
        size='small'
        width={80}
      />

      <Select
        options={[
          { value: 1, label: '전시상품ID' },
          { value: 2, label: '모델명' },
          { value: 3, label: '차량번호' },
        ]}
        value={1}
      />

      <Select
        options={[
          { value: 1, label: '서울' },
          { value: 2, label: '경기도' },
        ]}
        value={1}
        size='large'
        width={160}
      />
    </>
  )
}

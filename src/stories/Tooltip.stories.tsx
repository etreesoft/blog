import { ComponentMeta, ComponentStory } from '@storybook/react'

import Description from '@/components/Description'
import Tooltip from '@/components/Tooltip'
import Radio from '@/components/atomic/Radio'
import { Colors } from '@/styles/colors'

export default {
  title: 'Components/Tooltip',
  component: Tooltip,
  args: {},
} as ComponentMeta<typeof Tooltip>

export const Default: ComponentStory<typeof Tooltip> = (args) => {
  const { Tbody, Row, Th, Td } = Description

  return (
    <Description colWidth={[200, 'auto', 200, 'auto']} {...args}>
      <Tbody>
        <Row>
          <Th>
            <Tooltip label='제목 툴팁'>
              전시에 필요한 정보가 입력되지 않은 상품만 볼 수 있습니다.
            </Tooltip>
          </Th>
          <Td>
            <Tooltip
              label='본문 툴팁'
              style={{ color: Colors.RED }}
              iconColor='RED'
            >
              전시에 필요한 정보가 입력되지 않은 상품만 볼 수 있습니다.
            </Tooltip>
          </Td>
          <Th>등록일</Th>
          <Td>
            {[
              { label: '전체', value: 'all' },
              { label: '최근 7일', value: 'latest-7day' },
              { label: '기간 선택', value: 'date-input' },
            ].map(({ label, value }, i) => (
              <Radio
                key={i}
                label={label}
                value={value}
                defaultChecked={value === 'date-input'}
                name='register-date'
              />
            ))}
          </Td>
        </Row>
        <Row>
          <Th>
            <Tooltip
              label='내용이 긴 툴팁'
              style={{ color: Colors.RED }}
              iconColor='RED'
            >
              대한민국은 통일을 지향하며, 자유민주적 기본질서에 입각한 평화적
              통일 정책을 수립하고 이를 추진한다. 국가안전보장회의는 대통령이
              주재한다. 모든 국민은 그 보호하는 자녀에게 적어도 초등교육과
              법률이 정하는 교육을 받게 할 의무를 진다.
            </Tooltip>
          </Th>
          <Td>디 올 뉴 니로 2022</Td>
          <Th>차량번호</Th>
          <Td>12 라 1212</Td>
        </Row>
        <Row>
          <Th>출고일</Th>
          <Td>2022-12-13</Td>
          <Th>차량트립</Th>
          <Td>1.6 하이브리드 시그니처</Td>
        </Row>
        <Row>
          <Th>카테고리</Th>
          <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
          <Th>연식</Th>
          <Td>2023년식</Td>
        </Row>

        <Row>
          <Th>전시상품 ID</Th>
          <Td>displayID</Td>
          <Th>등록일</Th>
          <Td>
            {[
              { label: '전체', value: 'all' },
              { label: '최근 7일', value: 'latest-7day' },
              { label: '기간 선택', value: 'date-input' },
            ].map(({ label, value }, i) => (
              <Radio
                key={i}
                label={label}
                value={value}
                defaultChecked={value === 'date-input'}
                name='register-date'
              />
            ))}
          </Td>
        </Row>
        <Row>
          <Th>차량이름</Th>
          <Td>디 올 뉴 니로 2022</Td>
          <Th>차량번호</Th>
          <Td>12 라 1212</Td>
        </Row>
        <Row>
          <Th>출고일</Th>
          <Td>2022-12-13</Td>
          <Th>차량트립</Th>
          <Td>1.6 하이브리드 시그니처</Td>
        </Row>
        <Row>
          <Th>카테고리</Th>
          <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
          <Th>연식</Th>
          <Td>2023년식</Td>
        </Row>
        <Row>
          <Th>전시상품 ID</Th>
          <Td>displayID</Td>
          <Th>등록일</Th>
          <Td>
            {[
              { label: '전체', value: 'all' },
              { label: '최근 7일', value: 'latest-7day' },
              { label: '기간 선택', value: 'date-input' },
            ].map(({ label, value }, i) => (
              <Radio
                key={i}
                label={label}
                value={value}
                defaultChecked={value === 'date-input'}
                name='register-date'
              />
            ))}
          </Td>
        </Row>
        <Row>
          <Th>차량이름</Th>
          <Td>디 올 뉴 니로 2022</Td>
          <Th>차량번호</Th>
          <Td>12 라 1212</Td>
        </Row>
        <Row>
          <Th>출고일</Th>
          <Td>2022-12-13</Td>
          <Th>차량트립</Th>
          <Td>
            <Tooltip
              label='좌우 방향 확인용으로 만든 긴~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 텍스트'
              style={{ color: Colors.RED }}
              iconColor='RED'
            >
              전시에 필요한 정보가 입력되지 않은 상품만 볼 수 있습니다.
            </Tooltip>
          </Td>
        </Row>
        <Row>
          <Th>카테고리</Th>
          <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
          <Th>
            <Tooltip
              label={'상하 방향확인용 툴팁'}
              style={{ color: Colors.RED }}
              iconColor='RED'
            >
              전시에 필요한 정보가 입력되지 않은 상품만 볼 수 있습니다.
            </Tooltip>
          </Th>
          <Td>2023년식</Td>
        </Row>
        <Row>
          <Th>전시상품 ID</Th>
          <Td>displayID</Td>
          <Th>등록일</Th>
          <Td>
            {[
              { label: '전체', value: 'all' },
              { label: '최근 7일', value: 'latest-7day' },
              { label: '기간 선택', value: 'date-input' },
            ].map(({ label, value }, i) => (
              <Radio
                key={i}
                label={label}
                value={value}
                defaultChecked={value === 'date-input'}
                name='register-date'
              />
            ))}
          </Td>
        </Row>
        <Row>
          <Th>차량이름</Th>
          <Td>디 올 뉴 니로 2022</Td>
          <Th>차량번호</Th>
          <Td>12 라 1212</Td>
        </Row>
        <Row>
          <Th>출고일</Th>
          <Td>2022-12-13</Td>
          <Th>차량트립</Th>
          <Td>1.6 하이브리드 시그니처</Td>
        </Row>
        <Row>
          <Th>카테고리</Th>
          <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
          <Th>연식</Th>
          <Td>2023년식</Td>
        </Row>
      </Tbody>
    </Description>
  )
}

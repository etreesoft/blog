import { ComponentMeta, ComponentStory } from '@storybook/react'

import { useState } from 'react'

import Textarea from '@/components/atomic/Textarea'

export default {
  title: 'Atomic/Textarea',
  component: Textarea,
} as ComponentMeta<typeof Textarea>

export const Default: ComponentStory<typeof Textarea> = (args) => {
  const [value, setValue] = useState('홍길동')
  const [valueError, setValueError] = useState('이상한 값')

  return (
    <>
      <h1>Textarea</h1>
      <div className='explain'>
        <p>
          Textarea 컴포넌트의 높이를 설정할 때에는 <i>style - height</i> 속성이
          아닌, <i>rows</i> 속성으로 제어해주세요.
        </p>
        <ul>
          <li>
            <b>rows?:</b>number (default: 3)
          </li>
        </ul>
      </div>
      <h1>기본</h1>
      <Textarea
        placeholder='반려 사유를 입력해 주세요.'
        {...args}
        autoFocus
        rows={5}
      />
      <Textarea value={value} setValue={setValue} {...args} />
      <Textarea disabled value='입력 불가(disabled)' {...args} />
      <Textarea readOnly value='읽기 전용(readonly)' {...args} />

      <hr />

      <h1>오류</h1>
      <Textarea
        isValid={valueError === ''}
        value={valueError}
        setValue={setValueError}
        errorMessage='입력 형태가 올바르지 않습니다.'
        {...args}
      />
    </>
  )
}

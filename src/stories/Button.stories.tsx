import { ComponentMeta, ComponentStory } from '@storybook/react'

import Button from '@/components/atomic/Button'

export default {
  title: 'Atomic/Button',
  component: Button,
  args: {},
} as ComponentMeta<typeof Button>

export const Default: ComponentStory<typeof Button> = (args) => {
  return (
    <>
      <h1>Button</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>type?:</b> button | medium(default) | large
          </li>
          <li>
            <b>size?:</b> 46 | 44 | 32(default) | 26
          </li>
          <li>
            <b>primary?:</b> boolean(default: false)
          </li>
          <li>
            <b>icon?:</b> keyof typeof Icons (ex: 'excel')
          </li>
          <li>
            <b>iconPlacement?:</b> left(default) | right
          </li>
          <li>
            <b>iconColor?:</b> string(default: 'inherit')
          </li>
        </ul>
      </div>

      <h1>Size - 46</h1>

      <Button {...args} size={46}>
        버튼
      </Button>
      <Button {...args} disabled size={46}>
        Disabled
      </Button>
      <Button primary {...args} size={46}>
        Primary
      </Button>
      <Button primary {...args} disabled size={46}>
        Disabled
      </Button>

      <hr />

      <h1>Size - 44</h1>

      <Button {...args} size={44}>
        버튼
      </Button>
      <Button {...args} size={44} disabled>
        Disabled
      </Button>
      <Button primary {...args} size={44}>
        Primary
      </Button>
      <Button primary {...args} disabled size={44}>
        Disabled
      </Button>

      <hr />

      <h1>Size - 40</h1>

      <Button {...args} size={40}>
        버튼
      </Button>
      <Button {...args} size={40} disabled>
        Disabled
      </Button>
      <Button primary {...args} size={40}>
        Primary
      </Button>
      <Button primary {...args} disabled size={40}>
        Disabled
      </Button>

      <hr />

      <h1>Size - 32 (default)</h1>

      <Button {...args}>버튼</Button>
      <Button {...args} disabled>
        Disabled
      </Button>
      <Button primary {...args}>
        Primary
      </Button>
      <Button primary {...args} disabled>
        Disabled
      </Button>
      <hr />

      <h1>Size - 26</h1>

      <Button {...args} size={26}>
        버튼
      </Button>
      <Button {...args} size={26} disabled>
        Disabled
      </Button>
      <Button primary {...args} size={26}>
        Primary
      </Button>
      <Button primary {...args} size={26} disabled>
        Disabled
      </Button>

      <hr />

      <h1>icon</h1>

      <Button {...args} icon='excel'>
        Export
      </Button>

      <Button {...args} icon='excel' iconPlacement='right'>
        Export (right)
      </Button>

      <Button {...args} primary icon='excel'>
        Export
      </Button>

      <br />
      <br />

      <Button {...args} icon='reset' size={44} style={{ width: 192 }}>
        초기화
      </Button>

      <hr />
    </>
  )
}

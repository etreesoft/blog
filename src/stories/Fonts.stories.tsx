import { css } from '@emotion/react'

import { FontsKey, Fonts as fonts } from '@/styles/fonts'

export default {
  title: 'Foundation/Fonts',
  args: {},
}

export const Fonts = () => {
  const fontsKey = Object.keys(fonts)

  return (
    <>
      <h1>Fonts</h1>
      {fontsKey.map((font) => (
        <div css={styles.chip({ font: font as FontsKey })} key={font}>
          {font} - 헤는 가득 어머니, 둘 릴케 했던 별 계십니다.
        </div>
      ))}
    </>
  )
}

const styles = {
  chip: ({ font }: { font: FontsKey }) => css`
    margin: 20px 0;
    ${fonts[`${font}`]}
  `,
}

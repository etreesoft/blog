import { css } from '@emotion/react'
import { ComponentMeta, Story } from '@storybook/react'

import Accordion, { AccordionProps } from '@/components/Accordion'
import Description from '@/components/Description'
import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'

export default {
  title: 'Components/Accordion',
  component: Accordion,
  argTypes: {},
} as ComponentMeta<typeof Accordion>

const Template: Story<AccordionProps> = (args) => {
  const { Item } = Accordion
  const { Tbody, Row, Th, Td } = Description
  const IMAGE_MOCK = new Array(5).fill(null).map((_) => ({
    src: `${process.env.REACT_APP_CDN_BASE_URL}/images/mock/mock-car.png`,
    title: '정면 좌측 촬영 1',
    description: '차량에 대한 설명',
  }))
  return (
    <>
      <h1>Accordion Mode</h1>
      <Accordion css={styles.container}>
        <Item open>
          <Item.Header>자동차 일반 사양 정보 (요약) - opened</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              자동차의 일반적인 사양 정보를 제공합니다. (제조사, 자동차명,
              배기량, 사용연료를 포함한 자동차 사양)
            </div>
            <Description
              align='left'
              colWidth={[200, 'auto', 200, 'auto']}
              {...args}
            >
              <Tbody>
                <Row>
                  <Th>전시상품 ID</Th>
                  <Td>displayID</Td>
                  <Th>등록일</Th>
                  <Td>2022-01-23</Td>
                </Row>
                <Row>
                  <Th>차량이름</Th>
                  <Td>디 올 뉴 니로 2022</Td>
                  <Th>차량번호</Th>
                  <Td>12 라 1212</Td>
                </Row>
                <Row>
                  <Th>출고일</Th>
                  <Td>2022-12-13</Td>
                  <Th>차량트립</Th>
                  <Td>1.6 하이브리드 시그니처</Td>
                </Row>
                <Row>
                  <Th>카테고리</Th>
                  <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
                  <Th>연식</Th>
                  <Td>2023년식</Td>
                </Row>
              </Tbody>
            </Description>
          </Item.Body>
        </Item>
        <Item>
          <Item.Header>두번째 제목</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Est
              vitae corrupti quo rem labore cumque dolorem odit esse enim, quod
              perspiciatis sunt neque ad odio velit totam reiciendis mollitia
              quae?
            </div>
          </Item.Body>
        </Item>
        <Item>
          <Item.Header>세번째 제목</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              세번째 콘텐츠 Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Est vitae corrupti quo rem labore cumque dolorem odit esse
              enim, quod perspiciatis sunt neque ad odio velit totam reiciendis
              mollitia quae?
            </div>
          </Item.Body>
        </Item>
      </Accordion>

      <br />
      <br />
      <br />
      <br />

      <h1>Toggle Mode</h1>
      <Accordion toggle css={styles.container}>
        <Item open>
          <Item.Header>opened</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              첫번째 콘텐츠 Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Est vitae corrupti quo rem labore cumque dolorem odit esse
              enim, quod perspiciatis sunt neque ad odio velit totam reiciendis
              mollitia quae?
            </div>
          </Item.Body>
        </Item>
        <Item fixed>
          <Item.Header>fixed</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              두번째 콘텐츠 Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Est vitae corrupti quo rem labore cumque dolorem odit esse
              enim, quod perspiciatis sunt neque ad odio velit totam reiciendis
              mollitia quae?
            </div>
          </Item.Body>
        </Item>
        <Item>
          <Item.Header>세번째 제목</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              세번째 콘텐츠 Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Est vitae corrupti quo rem labore cumque dolorem odit esse
              enim, quod perspiciatis sunt neque ad odio velit totam reiciendis
              mollitia quae?
            </div>
            <Description
              align='left'
              colWidth={[200, 'auto', 200, 'auto']}
              {...args}
            >
              <Tbody>
                <Row>
                  <Th>전시상품 ID</Th>
                  <Td>displayID</Td>
                  <Th>등록일</Th>
                  <Td>2022-01-23</Td>
                </Row>
                <Row>
                  <Th>차량이름</Th>
                  <Td>디 올 뉴 니로 2022</Td>
                  <Th>차량번호</Th>
                  <Td>12 라 1212</Td>
                </Row>
                <Row>
                  <Th>출고일</Th>
                  <Td>2022-12-13</Td>
                  <Th>차량트립</Th>
                  <Td>1.6 하이브리드 시그니처</Td>
                </Row>
                <Row>
                  <Th>카테고리</Th>
                  <Td>SUV &gt; 니로 &gt; 디 올 뉴 니로</Td>
                  <Th>연식</Th>
                  <Td>2023년식</Td>
                </Row>
              </Tbody>
            </Description>
          </Item.Body>
        </Item>
        <Item>
          <Item.Header>네번째 제목</Item.Header>
          <Item.Body>
            <div css={styles.body}>
              네번째 콘텐츠 Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Est vitae corrupti quo rem labore cumque dolorem odit esse
              enim, quod perspiciatis sunt neque ad odio velit totam reiciendis
              mollitia quae?
            </div>
          </Item.Body>
        </Item>
      </Accordion>
    </>
  )
}

export const Default = Template.bind({})
Default.args = {}

const styles = {
  container: css`
    width: 90%;
    border: 1px solid ${Colors.COOL_GRAY_30};
    border-radius: 8px;
    padding: 24px;
  `,

  body: css`
    width: 100%;
    text-align: center;
    padding: 20px;
    border-bottom: 1px solid ${Colors.COOL_GRAY_20};
    ${Fonts.REGULAR_14}
  `,
}

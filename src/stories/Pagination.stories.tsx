import { useArgs } from '@storybook/addons'
import { ComponentMeta, ComponentStory } from '@storybook/react'

import Pagination from '@/components/Pagination'

export default {
  title: 'Data Display/Pagination',
  component: Pagination,
  args: {
    page: 1,
    totalPages: 25,
    range: 10,
    align: 'left',
  },
  parameters: {
    controls: {
      exclude: ['onChange'],
    },
  },
} as ComponentMeta<typeof Pagination>

const Template: ComponentStory<typeof Pagination> = (args) => {
  const [_args, updateArgs] = useArgs()
  const onChange = (page: number) => updateArgs({ page })

  return (
    <>
      <h1>
        Pagination
        {typeof args.totalPages === 'number'
          ? ' (totalPages: number type)'
          : ' (totalPages: object type)'}
      </h1>

      <div className='explain'>
        <ul>
          <li>
            <b>page:</b> 현재 페이지
          </li>
          {typeof args.totalPages === 'number' ? (
            <li>
              <b>totalPages:</b> 총 페이지 수
            </li>
          ) : (
            <li>
              <b>totalPages:</b> 총 페이지 수를 계산하기 위한 값
              <p>- totalCount: 총 아이템 갯수</p>
              <p>- perPage: 한 페이지 당 아이템 갯수</p>
            </li>
          )}
          <li>
            <b>pageRange:</b> 페이지 범위
          </li>
          <li>
            <b>align?:</b> <i>'left'| 'center' | 'right'(default)</i>
          </li>
        </ul>
      </div>

      <Pagination onChange={onChange} {..._args} />
    </>
  )
}

export const Default = Template.bind({})

export const TotalPagesSource = Template.bind({})
TotalPagesSource.args = {
  page: 1,
  totalPages: {
    totalCount: 250,
    perPage: 10,
  },
  range: 10,
}

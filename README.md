# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Command line instructions

You can also upload existing files from your computer using the instructions below.

### Create a new repository

```bash
git clone git@gitlab.com:etreesoft/etreesoft.git
cd etreesoft
```

### Git setup

```bash
git config user.name "etree_rnd"
git config user.email "etree_rnd@etreesoft.com"
```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


### `yarn run storybook`
Run the app in 'React Component Storybook'\
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

## Using Library

Below is a list of some libraries used in this project\
Please check the usage of these libraries

### emotion
https://emotion.sh/docs/introduction

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

const path = require('path')
const _ = require('lodash')

module.exports = {
  stories: [
    "../src/stories/*.stories.mdx",
    "../src/stories/*.stories.@(js|jsx|ts|tsx)"
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/preset-create-react-app"
  ],
  framework: "@storybook/react",
  core: {
    builder: "@storybook/builder-webpack5"
  },
  webpackFinal: async (config) => {
    config.resolve = _.merge(config.resolve, {
      alias: {
        '@': path.resolve(__dirname, '../src'),
      },
    })

    config.module.rules
      .flatMap((rule) => rule.oneOf || [])
      .filter(
        (subRule) => subRule.test instanceof RegExp && subRule.test.test('.tsx')
      )
      .forEach((subRule) =>
        subRule.include.push(path.resolve(__dirname, '../server'))
      )

    const babelRule = config.module.rules
      .find((rule) => rule.oneOf)
      .oneOf.find((rule) => rule.loader?.includes('babel-loader'))

    babelRule.options.presets = [
      ...babelRule.options.presets,
      [
        '@babel/preset-react',
        { runtime: 'automatic', importSource: '@emotion/react' },
      ],
    ]

    return config
  },
}
